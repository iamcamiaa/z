<?php 
use App\Sponsor;
use App\Patient;
use App\Donation;
$sponsor = Sponsor::where('userid', Auth::id())->get();
//--helpxpMoney
$helpxpMoney = DB::table('sponsors')
           ->join('donations', 'sponsors.sponsor_serial', '=', 'donations.sponsor_serial')->where('donations.patientid','=',null)->sum('sponsors.voucherValue');
//--strories money
$donatedtopatients = DB::table('sponsors')
                    ->join('donations', 'sponsors.sponsor_serial', '=', 'donations.sponsor_serial')->where('donations.patientid','!=',null)->sum('sponsors.voucherValue');
?>

<title>HELPXP</title>
<head>
<meta property="og:title" content=""/>
  <meta property="og:image" content=""/>
  <meta property="og:url" content=""/>
  <meta property="og:site_name" content=""/>
  <meta property="og:description" content=""/>
  <meta name="twitter:title" content="" />
  <meta name="twitter:image" content="" />
  <meta name="twitter:url" content="" />
  <meta name="twitter:card" content="" />

  <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
  <link rel="shortcut icon" href="favicon.ico">

  <!-- <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,700,300' rel='stylesheet' type='text/css'> -->
  <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular.min.js"></script>
  <!-- Animate.css -->
  <link rel="stylesheet" href="css/animate.css">
  <!-- Icomoon Icon Fonts-->
  <link rel="stylesheet" href="css/icomoon.css">
  <!-- Bootstrap  -->
  <link rel="stylesheet" href="css/bootstrap.css">
  <!-- Superfish -->
  <link rel="stylesheet" href="css/superfish.css">

  <link rel="stylesheet" href="css/style.css">

  <link rel="stylesheet" href="css/search.css">

  <!-- Modernizr JS -->
  <script src="js/modernizr-2.6.2.min.js"></script>
  <!-- FOR IE9 below -->
  <!--[if lt IE 9]>
  <script src="js/respond.min.js"></script>
  <![endif]-->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<meta name="userId" content="{{ Auth::check() ? Auth::user()->id : null }}">
<meta name="csrf-token" content="{{ csrf_token() }}">
<title>{{ config('app.name', 'Laravel') }}</title>
<script>
  window.Laravel =  <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
</script>



  <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<!-- ======= -->
  <!-- progress -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>


     <!--  carousel -->
 <link rel="stylesheet" href="css/slider.css">
  <script src="js/slider.js"></script>
  <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300,300italic,regular,italic,700,700italic&subset=latin-ext,greek-ext,cyrillic-ext,greek,vietnamese,latin,cyrillic" rel="stylesheet" type="text/css" />

<link rel="stylesheet" href="css/buttonReco.css">
  </head>
  <body>

  @guest
    <!-- <div id="fh5co-wrapper">
    <div id="fh5co-page">
    <div class="header-top">
      <div class="container">
        <div class="row">
          <div class="col-md-6 col-sm-6 text-left fh5co-link">
            <a href="#">FAQ</a>
            <a href="#">Forum</a>
            <a href="#">Contact</a>
          </div>
          <div class="col-md-6 col-sm-6 text-right fh5co-social">
            <a href="#" class="grow"><i class="icon-facebook2"></i></a>
            <a href="#" class="grow"><i class="icon-twitter2"></i></a>
            <a href="#" class="grow"><i class="icon-instagram2"></i></a>
          </div>
        </div>
      </div>
    </div> -->


    <header id="fh5co-header-section" class="sticky-banner">
      <div class="container">
        <div class="nav-header">
          <a href="#" class="js-fh5co-nav-toggle fh5co-nav-toggle dark"><i></i></a>
          <h1 id="fh5co-logo"><a href="{{ url('/home') }}">HELPXP</a></h1>
          <!-- START #fh5co-menu-wrap -->
          <nav id="fh5co-menu-wrap" role="navigation">

            <ul class="sf-menu" id="fh5co-primary-menu">   
              <li>
                <a href="{{ url('/home') }}">Home</a>
              </li>
              <li>
                <a href="#" class="fh5co-sub-ddown">Get Involved</a>
                <ul class="fh5co-sub-menu">
                  <li><a href="{{ url('/login') }}">Buy Voucher</a></li>
                     
                    
                    
                  <li><a href="{{ url('/login') }}">Fundraise</a></li>
                  <li><a href="{{ url('/login') }}">Apply Sponsorships</a></li>
                </ul>
              </li>
              <li><a href="{{ url('/about') }}">About</a></li>
              <li><a href="{{ route('login') }}">Login</a></li>
              <li><a href="{{ route('register') }}">Register</a></li>
            </ul>
          </nav>
        </div>
      </div>
    </header>
  @else
    
  <header id="fh5co-header-section" class="sticky-banner">
      <div class="container">
        <div class="nav-header">
          <a href="#" class="js-fh5co-nav-toggle fh5co-nav-toggle dark"><i></i></a>
          <h1 id="fh5co-logo"><a href="{{ url('/home') }}">HELPXP</a></h1>

<?php
$user = Auth::id();
$pnt = Patient::where('userid', $user)->where('status', 'approved')->orWhere('status', 'pending')->get()->count();
$patient = Patient::where('userid', $user)->get();
?>
   <div id="app">
   <nav id="fh5co-menu-wrap" role="navigation">
            <ul class="sf-menu" id="fh5co-primary-menu">
            <li>
                               <!-- search -->

                          <form action="{{url('/search')}}" method="POST">
                              {{csrf_field()}}
                         <input type="search" placeholder="Search" name="search">
                              </form>
 
                    <!-- end search -->
            </li>
              <li>
                <a href="{{ url('/home') }}">Home</a>
              </li>
              <li>
                <a href="#" class="fh5co-sub-ddown">Get Involved</a>
                <ul class="fh5co-sub-menu">

                <li><a href="#" class="fh5co-sub-ddown">Buy vouchers</a>
                  <ul class="fh5co-sub-menu"  style="width: 250px">
                    <li><a href="{{ url('/buyvoucher') }}/{{Auth::user()->id }}">Buy Voucher with Bank Deposit</a></li>
                    <li><a href="{{ url('/paypal') }}">Buy Voucher with Paypal</a></li>
                  </ul>
                </li>

                  <li><a href="{{ url('/donateAny') }}/{{Auth::user()->id }}">Fundraise</a></li>
         
                  <li><a href="{{ url('/patientsdetail') }}">Apply Sponsorships</a></li>
         
                </ul>
              </li>

  <notif v-bind:notifications="notifications"></notif>

              <li><a href="{{ url('/about') }}">About</a></li>
              
              <li><a class="fh5co-sub-ddown" href="">Hi <strong>{{ Auth::user()->fname }}</strong>!
              </a>
              <ul class="fh5co-sub-menu">
<li><a href="{{url('/mystory')}}"><img src="/images/script.png">&nbsp;My Story</a></li>
              <li>
<a href="{{url('/viewvoucher')}}" ><img src="/images/tickets.png">&nbsp;My Vouchers</a></li>
<li><a href="{{ url('/history') }}"><img src="/images/history.png">&nbsp;History</a></li>
              <li><a href="{{ route('logout')}}" onclick="event.preventDefault();
      document.getElementById('logout-form').submit();"><img src="/images/logout.png">&nbsp;{{ __('Logout') }}</a></li>
              </ul>
              </li>
            
      <form id="logout-form" action="{{ route('logout') }}" method="POST">
      @csrf
      </form>
  
    </ul>
  </nav>
  </div>
</div>
</div>
</header>

<!-- start sa not yet fin -->
<div class="fh5co-hero">
  

<div id="jssor_1" style="position:relative;margin:0 auto;top:0px;left:0px;width:1300px;height:600px;overflow:hidden;visibility:hidden;">
<!-- Loading Screen -->
<div data-u="loading" class="jssorl-009-spin" style="position:absolute;top:0px;left:0px;width:100%;height:100%;text-align:center;background-color:rgba(0,0,0,0.7);">
<img style="margin-top:-19px;position:relative;top:50%;width:38px;height:38px;" src="images/spin.svg" />
</div>
<div data-u="slides" style="cursor:default;position:relative;top:0px;left:0px;width:1300px;height:600px;overflow:hidden;">
<div data-p="225.00">
<img data-u="image" src="images/pov2.jpg" />
</div>
<div data-p="225.00">
<img data-u="image" src="images/pov1.jpg" />
</div>

<div data-p="225.00">
<img data-u="image" src="images/pov.jpg" />
<div style="position:absolute;top:300px;left:30px;width:480px;height:130px;font-family:'Roboto Condensed',sans-serif;font-size:30px;color:#000000;line-height:1.27;padding:5px 5px 5px 5px;box-sizing:border-box;margin-left: 50px">
  <h2 style="color: white;font-size:45px"><strong>Big</strong> or <strong>Small</strong> your cause matters.</h2>&nbsp;
  <span"><a class="btn btn-primary btn-lg" href="#">Donate Now</a></span></div>
</div>
</div>
<!-- Arrow Navigator -->
<div data-u="arrowleft" class="jssora051" style="width:65px;height:65px;top:0px;left:25px;" data-autocenter="2" data-scale="0.75" data-scale-left="0.75">
<svg viewbox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
<polyline class="a" points="11040,1920 4960,8000 11040,14080 "></polyline>
</svg>
</div>
<div data-u="arrowright" class="jssora051" style="width:65px;height:65px;top:0px;right:25px;" data-autocenter="2" data-scale="0.75" data-scale-right="0.75">
<svg viewbox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
<polyline class="a" points="4960,1920 11040,8000 4960,14080 "></polyline>
</svg>
</div>
</div>
<script type="text/javascript">jssor_1_slider_init();</script>

@endguest

<br>
<div class="container2">
 <div class="gallery">
  <div class="desc" style="color: green; font-size: 35pt; letter-spacing: 0.2em"> 
   P <strong>{{number_format(DB::table('sponsors')->where('status', 'donated')->orwhere('status', 'dd-hx')->orwhere('status', 'd-hx')->sum('voucherValue'))}}</strong> all time donations
  </div>
</div> 
</div><br><br>

<!-- helpxp money and all stories -->
<br>
<div class="container2">
 <div class="gallery">
  <div class="desc" style="color: green; font-size: 35pt;float: left"> 
   <center>P <strong>{{number_format($helpxpMoney)}}</strong><p>donated to HelpXp</p></center> 
  </div>
</div> 
</div>

<div class="container2">
 <div class="gallery">
  <div class="desc" style="color: green; font-size: 35pt"> 
   <center>P <strong>{{number_format($donatedtopatients)}}</strong><p>donated to all stories</p></center> 
  </div>
</div> 
</div>
<br><br><br><br><br>

<!-- end -->

<main class="py-4">
            @yield('content')
</main>

<script src="js/jquery.min.js"></script>
  <!-- jQuery Easing -->
  <script src="js/jquery.easing.1.3.js"></script>
  <!-- Bootstrap -->
  <script src="js/bootstrap.min.js"></script>
  <!-- Waypoints -->
  <script src="js/jquery.waypoints.min.js"></script>
  <script src="js/sticky.js"></script>

  <!-- Stellar -->
  <script src="js/jquery.stellar.min.js"></script>
  <!-- Superfish -->
  <!-- <script src="js/hoverIntent.js"></script> -->
  <script src="js/superfish.js"></script>
  
  <!-- Main JS -->
  <script src="js/main.js"></script>

<script src="//js.pusher.com/3.1/pusher.min.js"></script>
<script src="{{ asset('./js/app.js')}}"></script>
</body>
</html>
   