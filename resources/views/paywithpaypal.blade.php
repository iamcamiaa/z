<!DOCTYPE html>
<html>
<head>
	<title>Pay With Paypal</title>
	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
</head>
<body>
<div class="w3-container">


@if($message = Session::get('error_pay'))
<div class="w3-panel w3-red w3-display-container">
	<span onclick="this.parentElement.style.display='none'" class="w3-button w3-red w3-large w3-display-topright">&times;</span>
	<p>{!! $message !!}</p>
</div>
<?php Session::forget('error_pay'); ?> 
@endif


	<form class="w3-container w3-display-middle w3-card-4 w3-padding-16" method="post" id="payment-form" action="{!! URL::to('paypal') !!}">
	<div class="w3-container w3-teal w3-padding-16">Pay with Paypal</div>
	{{ csrf_field() }}
	<h2 class="w3-text-blue">Payment Form</h2>
	<p>Demo Paypal Form - Integrating Paypal in Laravel</p>
	<label class="w3-text-blue"><b>Enter Amount</b></label>
	<input type="text" name="amount" class="w3-input w3-border" id="amount" ><br>
	<button class="w3-btn w3-blue">Pay with Paypal</button><br><br>
	@if(Auth::user()->paypal_balance != 0)
	<a class="btn btn-link w3-text-blue" href="{{ url('/buyvoucherpaypal') }}">Already have balance? Buy Voucher</a>
	@endif
	</form>
</div>
</body>
</html>