@extends('layouts.historyui')
@section('content')
<?php
use App\Donation;

?>

<head>
    <title>History</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->  
    <link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/perfect-scrollbar/perfect-scrollbar.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="css/util.css">
    <link rel="stylesheet" type="text/css" href="css/main.css">
<!--===============================================================================================-->

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>


<script>
setInterval(function(){
    $(document).ready(function (){
      var to = "";
      var div=$(this).parent();
        $.ajax({
            type: 'GET',
            url: '/total',
            data:{
                '_token': $('input[name=_token]').val()
            },
            success:function(data){
                if(data.length == 0){
                    to+=`<center><h1>No record as of now</h1></center>`;
                    $('#patientTotal').html(to);
                }
                for(var i=0; i<data.length; i++){
                    var date = data[i].created_at;
                    var dt = new Date(date);
                    var redeem = data[i].TotalRedeem - data[i].redeemed;
                    var options = { year: 'numeric', month: 'long', day: 'numeric' };
                    to+=`
                    <form action="{{url('/confirm')}}/`+data[i].patientid+`">
                        <table>
                            <tbody>
                                <tr class="row100 body">
                                    <td class="cell100 column1">`+dt.toLocaleDateString("en-US", options)+`</td>
                                    <td class="cell100 column2">`+data[i].goal+`</td>
                                    
                                        <td class="cell100 column3"><input type="submit" class="btn btn-primary" name="Redeem" value="Redeem" ></td>
                                    
                                        
                                     <td class="cell100 column4"><div id="">`+redeem+`</td>
                                </tr>         
                            </tbody>
                        </table>
                    </form>
                    `;
                
                }$('#patientTotal').html(to);
                 
                console.log(to);
            },
            error:function(){
            }
        });
    });
},2000);
</script>


<script>
// setInterval(function(){
    
        function donate(){
        var from = $('#from').val();
        var toDate = $('#to').val();

      var to = "";
      var div=$(this).parent();
        $.ajax({
            type: 'GET',
            url: '/filterDonations/'+from+'/'+toDate,
            success:function(data){
                for(var i=0; i<data.length; i++){
                    var date = data[i].updated_at;
                    var dt = new Date(date);
                    var options = { year: 'numeric', month: 'long', day: 'numeric' };
                   
                    to +=`
                    <table >
                            <tbody >
                       <tr class="row100 body">
                                    <td class="cell100 column1">`+dt.toLocaleDateString("en-US", options)+`</td>
                                    <td class="cell100 column2">`+data[i].voucherValue+`</td>
                                  
                                   <td class="cell100 column3">`+data[i].fname+` `+data[i].lname+`</td>
                                  
                                    
                                  
                                </tr>
                                </tbody>          
                        </table>
                    `;                    
                                
                }               
                 $('#donations').html(to);
                 
                console.log(to);
            },
            error:function(){
            }
        });
        }
// },3000);

</script>
 
<style>
    body {
       background-image: url('/images/bgg.png');
        top right no-repeat; 
        background-attachment:fixed;
        background-size: cover;
        margin-top: 0px;
    }
</style>

<div style="background-color: #fff; margin-right: 110px; margin-left: 70px; margin-top: 10px; border-radius: 15px 15px;">
<div class="limiter">
        <div class="container-table100">
            <div class="wrap-table100"><p style="font-size: 15pt; font-family: Lato-Bold; color: black">Current Story</p>
                <div class="table100 ver5 m-b-110">
                    <div class="table100-head">
                        <table>
                            <thead>
                                <tr class="row100 head">
                                    <th class="cell100 column1">Date</th>
                                    <th class="cell100 column2">Goal</th>
                                    <th class="cell100 column3"></th>
                                    <th class="cell100 column4">Total</th>
                                </tr>
                            </thead>
                        </table>
                    </div>

                   
                    
                    <div class="table100-body js-pscroll" id="patientTotal">
                       
                     </div>
                  
                
            </div>
                


    <div class="wrap-table100"><p style="font-size: 15pt; font-family: Lato-Bold; color: black">Redeemed and Success Stories</p>
        <div class="table100 ver5 m-b-110">
                    <div class="table100-head">
                        <table>
                            <thead>
                                <tr class="row100 head">
                                    <th class="cell100 column1">Date</th>
                                    <th class="cell100 column2">Goal</th>
                                    <th class="cell100 column3">Amount Redeemed</th>
                                </tr>
                            </thead>
                        </table>
                    </div>

                     @foreach ($redeemdetails as $redeem)

                    <div class="table100-body js-pscroll">
                        <table>
                            <tbody>
                                <tr class="row100 body">
                                    <td class="cell100 column1">{{$redeem->created_at->format('F d, Y')}}</td>
                                    <td class="cell100 column2">{{$redeem->goal}}</td>
                                    <td class="cell100 column3">{{$redeem->TotalRedeem}}</td>
                                </tr>
                                 @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

    <div class="wrap-table100">
 
    From: <input type="date" name="from" id="from">
    To: <input type="date" name="to" id="to">
    <input type="submit" id="filter" value="filter" onclick="donate()">

    <p style="font-size: 15pt; font-family: Lato-Bold; color: black">History of Donations</p>
        <div class="table100 ver5 m-b-110">
                    <div class="table100-head">

                    <center>@foreach ($helpxp as $h)
                    {{$h->sum}}
                    total donation to helpxp
                    @endforeach</center>
                        <table>
                            <thead>
                                <tr class="row100 head">
                                    <th class="cell100 column1">Date</th>
                                    <th class="cell100 column2">Amount Needed</th>
                                    <th class="cell100 column3">To whom</th>
                                </tr>
                            </thead>
                        </table>
                    </div>





                    <div class="table100-body js-pscroll">
                        <table id="donations">
                            <tbody >
                            @foreach ($sponsorCollect as $sponsors)
                                <tr class="row100 body">
                                    <td class="cell100 column1">{{$sponsors->updated_at->format('F d, Y')}}</td>
                                    <td class="cell100 column2">{{$sponsors->voucherValue}}</td>
                                    @foreach ($sponsors->donation as $sponsors)
                                    @if($sponsors->patient == null)
                                    <td class="cell100 column3">HelpXP</td>
                                    @else
                                    <td class="cell100 column4">{{$sponsors->patient->userName->fname}} {{$sponsors->patient->userName->lname}}</td>
                                    @endif
                                    
                                    @endforeach
                                </tr>
                                 @endforeach
                            </tbody>
                         
                        </table>
                    </div>
                </div>
            </div>  
        </div>
    </div>
</div>


<div class="modal fade" id="success" role="dialog">
    <div class="modal-dialog" id="container" style="width:0 auto; padding: 10px;">    
      <div class="modal-content" style="background-color: #a4b0be; border-radius: 0px;">
        <div class="modal-body">
          <center><h3 id="msg" style="color: white; text-decoration: underline orange;"></h3>
          <br>
          <button type="button" class="btn btn-default" data-dismiss="modal" style="background-color: orange; font-weight: bold; color: white;">Okay</button></center>
          </div>
      </div>     
    </div>
  </div>

  @if(Session::has('success'))
    <script>
    var change = "";
$(document).ready(function() {
    change = `<h3 id="msg" style="color: white; text-decoration: underline orange;">You may claim your voucher to the nearest branch!</h3>`;
    $('#msg').html(change);
    $('#success').modal('show');
});
</script><?php Session::forget('success'); ?>
@endif
<br>
@endsection
