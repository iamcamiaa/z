<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="css/app.css">
</head>
<body>
	<h2>History</h2>
	<table cellpadding="30px" cellspacing="0">
	
	<th>Name</th>
	<th>Amount</th>
	<th>Date</th>

	<?php 
	foreach ($user as $sponsors) {
		?>
		<tr>
			<td>{{ $sponsors->user->name }}</td>
			<td>{{ $sponsors->amountDonated }}</td>
			<td>{{ $sponsors->created_at->format('F-d-Y') }}</td>

		<?php
	} ?>
</table>

</body>
</html>