 @extends('layouts.patient')

@section('content')

<!-- <div class="about" style="width: 25%; height: auto">
  <div class="desc" style="background-color: lightgreen; border: 1px solid #ccc">
     requirements:
     <br>original copy of medical abstract
     <br>medical certification
     <br>Valid ID's (voter's ID, passport, etc.)
     <br>hospital bill
     <br>official quotation of breakdown of expenses
  </div>
</div>  -->

<style>
    body {
        background-image: url('/images/bgg.png');
        top right no-repeat; 
        background-attachment:fixed;
        background-size: cover;
        margin-top: 0px;
    }
    input[type=text]{
        width: 925px;
        height: 40px;
    }
    * {
    box-sizing: border-box;
}

/* Create two equal columns that floats next to each other */
.column {
    float: left;
    width: 50%;
    padding: 10px;
    height: 300px; /* Should be removed. Only for demonstration */
}

/* Clear floats after the columns */
.row:after {
    content: "";
    display: table;
    clear: both;
}
</style>

<head>

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="/css/apply-form.css">
    <script src="/js/apply-js.js"></script>
    <link rel="stylesheet" href="/css/fileupload.css">
    <script src="/js/fileupload.js"></script>
    <script class="jsbin" src="https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
   <!--  <link rel="stylesheet" href="/css/form-basic.css"> -->

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>

<br><br>

    <div class="main-content" >

    <!-- angel edited -->
    <form class="form-basic" action="{{url('/singlelist')}}" method="post" enctype="multipart/form-data" style="background-color: #F9F9F9">
        {{csrf_field()}}           

<div id="main-page">
  <div class="maincontent">
    <h1>Share your story</h1>
                      
                        <h2>STEP 1</h2>
                    <div style="text-align:left;padding-left:15%;font-size: 13pt;padding-top: 15">
                        <label>Goal Title</label>
                        <p style="font-size:12px;color:black;float:right;padding-right:15%"><span>*Title of your story</span></p>
                        <input type="text" name="title" cols="100" style="border-color:#1FB264">                        
                    </div>

                    <div style="text-align:left;padding-left:15%;font-size: 13pt;padding-top: 15">
                    <label>Your story</label>
                    <p style="font-size:12px;color:black;float:right;padding-right:15%"><span id="display_count">0</span>/250 words</p>
                    <textarea name="story" id="countWord" cols="100" rows="15" style="padding-left:50px;border-color:#1FB264"></textarea><br>
                    
                     </div>

                    <div style="text-align:left;padding-left:15%;font-size: 13pt;padding-top: 15">
                        <label>Goal Amount</label>
                        <p style="font-size:12px;color:black;float:right;padding-right:15%"><span>*Amount you want to raise</span></p>
                        <input type="text" name="goal" style="border-color:#1FB264">     
                    </div>
<br>
  <a class="mainlink" style="float:right;padding-left:5px;padding-right:2px">NEXT &rarr;</a>
<br><br><br><br><br>
</div>
</div>

<div id="second-page">
  <div class="secondcontent">
    <h1>Share your story</h1>

                        <h2>STEP 2</h2>

    <div class="file-upload">
    <button class="file-upload-btn" type="button" onclick="$('.file-upload-input').trigger( 'click' )">Upload profile photo of your story</button>

    <div class="image-upload-wrap">
    <input class="file-upload-input" name="profile" type='file' onchange="readURL(this);" accept="image/*" />
    <div class="drag-text">
      <h3>Select upload photo</h3>
    </div>
  </div>
  <div class="file-upload-content">
    <img class="file-upload-image" src="#" alt="your image" />
    <div class="image-title-wrap">
      <button type="button" onclick="removeUpload()" class="remove-image">Remove <span class="image-title">Uploaded Image</span></button>
    </div>
  </div>
</div>
                
        <!--     <div style="text-align:left;padding-left:15%;font-size: 13pt;padding-top: 15">
                <label>Upload photo of your story</label>
                <p style="font-size:12px;color:black;float:right;padding-right:15%"><span>*This will serve as a profile picture of your story</span></p>
                <input type="file" name="profile">           
            </div> -->

            <div style="text-align:left;padding-left:15%;font-size: 13pt;padding-top: 15">
                <label>Beneficiary Name</label>
                <p style="font-size:12px;color:black;float:right;padding-right:15%"><span>*Complete name of the beneficiary</span></p> 
                <input type="text" name="bname" style="border-color:#1FB264">
            </div>

            <div style="text-align:left;padding-left:15%;font-size: 13pt;padding-top: 15">
                <label>Cause of Admission</label>
                 <p style="font-size:12px;color:black;float:right;padding-right:15%"><span>*Illnes of the beneficiary</span></p>
                <input type="text" name="illness" style="border-color:#1FB264">
                
            </div>

              <div style="text-align:left;padding-left:15%;font-size: 13pt;padding-top: 15">
                <label>Condition</label>
                    <select name="condition" style="border-color:#1FB264">
                        <option value="1">fair</option>
                        <option value="2">undetermined</option>
                        <option value="3">critical but stable</option>
                        <option value="4">serious</option>
                        <option value="5">critical</option>                      
                    </select>
            </div>
<br>
  <a class="secondlink" style="float:right;padding-left:5px;padding-right:2px">NEXT &rarr;</a>
<br><br><br><br><br>
</div>
</div>

<div id="next-page">
  <div class="nextcontent">

<!-- <div style="float: left;border: 1px solid #e65c00;margin-left: 20px;padding: 15px;color:#ffa366">
          <h3 style="text-align: center"><strong style="font-size: 50px">REQUIRED!</h3>
          <p style="font-size:10px;text-align: center">Failure to comply requirements may result to<br> unprocessed  application form.</p>
          <p>* Medical Abstract <br>* Medical Certificate <br> * Valid Id <br> * Hospital Bill <br>* Breakdown of Expenses</p>   
        </div> -->

    <h1>Share your story</h1>

                        <h2>STEP 3</h2>

<!-- first row -->
<!-- <div class="row">

  <div class="column" style="background-color:#e6fff2;">
    <div class="file-upload">
        <button class="file-upload-btn" type="button" onclick="$('.file-upload-input').trigger( 'click' )">Medical Abstract</button>

        <div class="image-upload-wrap">
        <input class="file-upload-input" name="medicalAbstract" type='file' onchange="readURL(this);" accept="image/*" />
            <div class="drag-text">
            <h3>Upload photo</h3>
            </div>
        </div>

        <div class="file-upload-content">
        <img class="file-upload-image" src="#" alt="your image" />
            <div class="image-title-wrap">
            <button type="button" onclick="removeUpload()" class="remove-image">Remove <span class="image-title">Uploaded Image</span></button>
            </div>
        </div>

    </div>
  </div>


  <div class="column" style="background-color:#e6fff2;">
        <div class="file-upload">
        <button class="file-upload-btn" type="button" onclick="$('.file-upload-input').trigger( 'click' )">Medical Certificate</button>

        <div class="image-upload-wrap">
        <input class="file-upload-input" name="medicalCertificate" type='file' onchange="readURL(this);" accept="image/*" />
            <div class="drag-text">
            <h3>Upload photo</h3>
            </div>
        </div>

        <div class="file-upload-content">
        <img class="file-upload-image" src="#" alt="your image" />
            <div class="image-title-wrap">
            <button type="button" onclick="removeUpload()" class="remove-image">Remove <span class="image-title">Uploaded Image</span></button>
            </div>
        </div>

    </div>
  </div>

</div>
<!-- end of first row -->

<!-- start of second row -->
<!-- <div class="row">

  <div class="column" style="background-color:#e6fff2;">
    <div class="file-upload">
        <button class="file-upload-btn" type="button" onclick="$('.file-upload-input').trigger( 'click' )">Valid ID</button>

        <div class="image-upload-wrap">
        <input class="file-upload-input" name="validID" type='file' onchange="readURL(this);" accept="image/*" />
            <div class="drag-text">
            <h3>Upload photo</h3>
            </div>
        </div>

        <div class="file-upload-content">
        <img class="file-upload-image" src="#" alt="your image" />
            <div class="image-title-wrap">
            <button type="button" onclick="removeUpload()" class="remove-image">Remove <span class="image-title">Uploaded Image</span></button>
            </div>
        </div>

    </div>
  </div>


  <div class="column" style="background-color:#e6fff2;">
        <div class="file-upload">
        <button class="file-upload-btn" type="button" onclick="$('.file-upload-input').trigger( 'click' )">Hospital Bill</button>

        <div class="image-upload-wrap">
        <input class="file-upload-input" name="hospitalBill" type='file' onchange="readURL(this);" accept="image/*" />
            <div class="drag-text">
            <h3>Upload photo</h3>
            </div>
        </div>

        <div class="file-upload-content">
        <img class="file-upload-image" src="#" alt="your image" />
            <div class="image-title-wrap">
            <button type="button" onclick="removeUpload()" class="remove-image">Remove <span class="image-title">Uploaded Image</span></button>
            </div>
        </div>

    </div>
  </div>

</div> -->
<!-- second row end -->

<!-- third row -->
<!-- <div style="float: center">

  <div style="background-color:#e6fff2;">
    <div class="file-upload">
        <button class="file-upload-btn" type="button" onclick="$('.file-upload-input').trigger( 'click' )">Breakdown of Expenses</button>

        <div class="image-upload-wrap">
        <input class="file-upload-input" name="breakdownExpenses" type='file' onchange="readURL(this);" accept="image/*" />
            <div class="drag-text">
            <h3>Upload photo</h3>
            </div>
        </div>

        <div class="file-upload-content">
        <img class="file-upload-image" src="#" alt="your image" />
            <div class="image-title-wrap">
            <button type="button" onclick="removeUpload()" class="remove-image">Remove <span class="image-title">Uploaded Image</span></button>
            </div>
        </div>

    </div><br>
  </div>

</div> --> -->
<!-- end third -->
            
            <div style="text-align: center;font-size: 13pt;padding-top: 15">
                <label>
                    <span>Medical Abstract</span>
                    <span style="padding-left: 0;"><input type="file" name="medicalAbstract"></span>
                </label>
            </div>
            <div style="text-align: center;font-size: 13pt;padding-top: 15">
                <label>
                    <span>Medical Certification</span>
                    <span style="padding-left: 0;"><input type="file" name="medicalCertificate"></span>
                </label>
            </div>
            <div style="text-align: center;font-size: 13pt;padding-top: 15">
                <label>
                    <span>Valid ID</span>
                    <span style="padding-left: 0;"><input type="file" name="validID"></span>
                </label>
            </div>
            <div style="text-align: center;font-size: 13pt;padding-top: 15">
                <label>
                    <span>Hospital Bill</span>
                    <span style="padding-left: 0;"><input type="file" name="hospitalBill"></span>
                </label>
            </div>
            <div style="text-align: center;font-size: 13pt;padding-top: 15">
                <label>
                    <span>Breakdown of Expenses</span>
                    <span style="padding-left: 0;"><input type="file" name="breakdownExpenses"></span>
                </label>
            </div>
             <div >
                <center><label>
                    <span style="font-color: white"><input type="submit" class="btn btn-primary" value="Submit">
                    <input style="color: white" type="reset" class="btn btn-danger" value="Reset"></span>
                </label></center>
            </div>
<br>
    <a class="nextlink" style="float:right;"> &larr; GO BACK</a>
<br><br><br><br><br>
</div>
</div>

</div>
</form>

   <div class="modal fade" id="success" role="dialog">
    <div class="modal-dialog" id="container" style="width:0 auto; padding: 10px;">    
      <div class="modal-content" style="background-color: #a4b0be; border-radius: 0px;">
        <div class="modal-body">
          <center><h3 id="msg" style="color: white; text-decoration: underline orange;"></h3>
          <br>
          <button type="button" class="btn btn-default" data-dismiss="modal" style="background-color: orange; font-weight: bold; color: white;">Okay</button></center>
          </div>
      </div>     
    </div>
  </div>

@if(Session::has('error'))
    <script>
    var change = "";
$(document).ready(function() {
    change = `<h3 id="msg" style="color: white; text-decoration: underline orange;">Incomplete Requirements</h3>`;
    $('#msg').html(change);
    $('#success').modal('show');
});
</script><?php Session::forget('success'); ?>    
@endif

</body>

</html>

<script>
    $(document).ready(function() {
  $("#countWord").on('keyup', function() {
    var words = this.value.match(/\S+/g).length;

    if (words > 250) {
      // Split the string on first 200 words and rejoin on spaces
      var trimmed = $(this).val().split(/\s+/, 250).join(" ");
      // Add a space at the end to make sure more typing creates new words
      $(this).val(trimmed + " ");
    }
    else {
      $('#display_count').text(words);
      $('#word_left').text(250-words);
    }
  });
}); 
</script>



@endsection 




