@extends('layouts.raise')
@section('content')


<head>


    <link rel="stylesheet" href="/css/form-basic.css">
<style>
    body {
        background-image: url('/images/bgg.png');
        top right no-repeat; 
        background-size: cover;
        margin-top: 0px;
    }
    #x{
        font-size: 40px;
        color: black;
    }
    #popup {
    position: absolute;
    padding: 40px;
    left: 50%;
    top: 50%;
    margin-top: -100px;
    margin-left: -200px;
    background-color: #f7e5c5;
    border-radius: 20px;
    border: 3px solid black;
    }
    #div{
      background-color:#f7e5c5 ;
      padding: 10px;
      width: 350px; 
      height: 235px;
      margin-left: 5px;
      border: 3px solid black;
      border-radius: 20px;
    }
    #divf{
      float: right;
      background-color:#f7e5c5 ;
      padding: 5px;
      width: 1300px; 
      margin-right: 5px;
      margin-top: -230px;
      display: inline-block;
      border: 3px solid black;
      border-radius: 20px;
      margin-bottom: 30px;
    }
    #container{
      margin-top: 300px;
    }

    #pdetails, #final{
      font-family: arial, sans-serif;
      border-collapse: collapse;
      width: 100%;
      color: black;
    }
    #det{
      height: 650px;
      overflow: auto;
    }
  #patients,  #highestNeed, #longestDuration, #condition{
      font-family: arial, sans-serif;
      border-collapse: collapse;
      color: black;
      display: inline-table;
  }

  #td, th {
      border: 1px solid #dddddd;
      text-align: left;
      padding: 8px;
      color: black;
  }
  tr:nth-child(even) {
      background-color: #dddddd;
  }
  #noresult{
      margin-left: 500px;
      font-weight: bold;
      font-size: 40px;
  }
  #tbl{
    background-color: white;
    padding: 10px;
  }
  #vo{
    background-color: #ffeaa7;
    padding: 20px;
    width: 35%;
    margin-top: 30px;
    display: inline-table;
  }
  #voo{
    background-color: #ffeaa7;
    padding: 20px;
    width: 70%;
    margin-top: 30px;
    display: inline-table;
    margin-bottom: 30px;
  }
  .e1{
    background-color: #ffeaa7;
    padding: 10px;
    color: black;
    font-weight: bold;
    font-size: 40px;
  }

</style>
</head>
<center>
<span id="errorc1">
<div class="vi">
    <table id="patients"></table>
</div>
</span>
<div class="v">
  <table id="longestDuration"></table>
</div>
<div class="v">
  <table id="highestNeed"></table>
</div>
<div class="v">
  <table id="condition"></table>
</div>
<div class="vv">
<table id="final"></table>
</div>
</center>
<div id="div">
<center><h3 style="font-weight: bold">Criteria</h3></center>
<table>@foreach($criteria as $cri)
  <tr>
    <td>{{$cri->name}}</td>
    <td>&nbsp;&nbsp;&nbsp;</td>
    <td><input type="number" step="5" min="0" style="width: 2.5em" value="{{$cri->percentage}}" id="change{{$cri->criteria_id}}">%</td>
  </tr>@endforeach
</table><br>
<center><button onclick="percentchange()" style="color: white" class="btn btn-primary">Change Percenatage/s</button></center><br><br>
</div>
<div id="divf">
<script type="text/javascript">
     $(document).ready(function (){
      var to = "";
      var total = "";
      var div=$(this).parent();
        $.ajax({
            type: 'GET',
            url: '/helpVoucher',
            data:{
                '_token': $('p[name=_token]').val()
            },
            success:function(data){

                for(var i=0; i<data.length; i++){

                    var image =  data[i].count+'&nbsp;&nbsp;'+'<img src="'+data[i].value+'" width = 300/>'+'&nbsp;&nbsp;';
                    $('#x').append(image);
                }if(data.length == 0){
                  console.log("no vouchers");
                  $('#x').html("No Voucher/s Available");
                } 
                
                console.log(data);
            },
            error:function(){
            }
        });
    });
</script><br><br>
<center><p style="font-weight: bold; " id="x"></p></center>
  @if($donate->count() == 0)
    <div id="noresult">NO RESULT</div>
  @else
  <div id="pntdetails">
    <table id="pdetails">
  <tr id="tr">
    <th>DATE</th>
    <th>PATIENT'S NAME</th>
    <th>TOTAL DONATION</th>
    <th></th>
  </tr>
  @foreach($donate as $pnt)
  <tr >
    <td id="td">{{$pnt->created_at->format('F d, Y')}}</td>
    <td>{{$pnt->patientname}}</td>
    <td>{{$pnt->total}}</td>
    <td><button class="btn-info" data-toggle ="modal" data-target="#p{{$pnt->patientid}}">Sponsors</button></td>
  </tr>
  @endforeach
</table>

<!-- modal for sponsors -->
@foreach($donate as $pnt)
<div class="modal fade" id="p{{$pnt->patientid}}" role="dialog">
    <div class="modal-dialog" id="container" style="width: 550px; margin-top: 40px;">    
      <div class="modal-content" style="background-color: #ffeaa7">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <center><h4 class="modal-title" style="color: black; text-decoration: underline orange; font-weight: bold">SPONSORS</h4></center>
        </div>
        <div class="modal-body" id="det">
        <center>
          <table width="100%">
            <tr id="tr">
              <th>Date</th>
              <th>Donor's Name</th>
              <th>Voucher Value</th>
            </tr>
            @foreach($pnt->voucher as $s)
            <tr>
              <td>{{$s->sponsor->created_at->format('F d, Y')}}</td>
              <td id="td">{{$s->sponsor->user->fname}} {{$s->sponsor->user->lname}}</td>
              <td>{{$s->sponsor->voucherValue}}</td>
            </tr>
            @endforeach
          </table>
        </center>
        </div>
      </div>     
    </div>
  </div>
@endforeach
</div>
  <!-- end modal -->
  @endif
<br>
<button  onclick="gcode()" class="btn btn-primary" data-toggle="modal" data-target="#myModal">Recommend</button>
<button onclick="recommend()">reco</button>
<a href="{{ url('/getrecoresults') }}" class="btn btn-link" style="float: right;">View possible results</a>
<br><br></div>
<!-- modal OPT -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog" id="container" style="width: 400px; border:3px solid grey; border-radius: 10px;">    
      <div class="modal-content" style="background-color: #a4b0be">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title" style="color: white; text-decoration: underline orange;">Input Generated Code</h4>
        </div>
        <div class="modal-body">
          <p id="expire" style="color: white">Code:&nbsp;<input type="text" id="gencode" style="color: black"></p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal" onclick="getcode()" style="background-color: orange; font-weight: bold; color: white">Generate</button>
        </div>
      </div>     
    </div>
  </div>

<div class="modal fade" id="success" role="dialog">
    <div class="modal-dialog" id="container" style="width:0 auto; padding: 10px;">    
      <div class="modal-content" style="background-color: #a4b0be; border-radius: 0px;">
        <div class="modal-body">
          <center><h3 id="msg" style="color: white; text-decoration: underline orange;">PERCENTAGE CHANGED!</h3><br>
          <button type="button" class="btn btn-default" data-dismiss="modal" style="background-color: orange; font-weight: bold; color: white;">Okay</button></center>
          </div>
      </div>     
    </div>
  </div>

<!-- end of Modal -->

<br><br>


<script>
  function percentchange(){
    var c1 = document.getElementById('change1').value;
    var c2 = document.getElementById('change2').value;
    var c3 = document.getElementById('change3').value;
    var c4 = document.getElementById('change4').value;
    var t1 = parseInt(c1);
    var t2 = parseInt(c2);
    var t3 = parseInt(c3);
    var t4 = parseInt(c4);
    var total = t1 + t2 + t3 + t4;
    var change ="";
    if(total == 100){
        $.ajax({
        type: "GET",
        url: '/percentChange/'+c1+'/'+c2+'/'+c3+'/'+c4,
          success: function(data){
            console.log(data);
            $("#success").modal('show');
          }
        });
    }else if(total > 100){
      change = `<h3 id="msg" style="color: white; text-decoration: underline orange;">Your percentage is greater than 100%.</h3>`;
      $('#msg').html(change);
      $("#success").modal('show');
    }else if(total < 100){
      change = `<h3 id="msg" style="color: white; text-decoration: underline orange;">Your percentage is lower than 100%.</h3>`;
      $('#msg').html(change);
      $("#success").modal('show');
    }
  }
</script>


<script>
var mytim;
function generateNewCode() {
mytim = setInterval(function(){
  var conf = confirm('code has expire');
  if(conf == true){
    $.ajax({
      type: 'GET',
      url: '/generateCode',
      success:function(data){
          console.log("new Code"+data);
      }
    });  
  }else{
    clearInterval(mytim);
  }
}, 5 * 60000);
};

function gcode(){
  $.ajax({
        type: 'GET',
        url: '/generateCode',
        success:function(data){
          console.log(data);
        }
  });
  generateNewCode();
};

function getcode(){
  var getcode = document.getElementById('gencode').value;
  $.ajax({
        type: 'GET',
        url: '/generateNewCode/'+getcode,
        success:function(data){
          console.log(data);
          if(data == "true"){
            clearInterval(mytim);
            recommend();
          }
        }
  });
}
</script>

<script>
function recommend(){
      var to = "";
      var text = "";
      var text1 = "";
      var text2 = "";
      var text3 = "";
      var sum_up = "";
      var modals = "";
        $.ajax({
            type: 'GET',
            url: '/recommending',
            success:function(data){
            console.log(data[4].length);
              $(".v").attr("id", "vo");
              $(".vv").attr("id", "voo");
            if(data[5] != 0){
            if(data[0] != 0){
              $(".vi").attr("id", "vo");
            text +=   `<tr>
                          <th colspan="4"><center>Most Patient Donated as Sponsor</center></th>
                        </tr>
                        <tr id="tr">
                          <th>Rank</th>
                          <th>Patient's Name</th>
                          <th>Total of number donated</th>
                          <th>Points Given</th>
                        </tr>`;   
            for(var i=0; i<data[0].length; i++){
                text +=`<tr>
                      <td id="td">`+data[0][i].rank+`</td>
                        <td id="td">`+data[0][i].patientname+`</td>
                        <td id="td">`+data[0][i].count+`</td>
                        <td id="td">`+data[0][i].points+`%</td>
                        </tr>`;
            }$('#patients').html(text);
            }else{
              $("#errorc1").attr("class", "e1");
              $('#errorc1').html("No Patient/s under this criteria");
            }

            if(data[1] != 0){
            text1 +=   `<tr>
                          <th colspan="4"><center>Patient with Longest Duration</center></th>
                        </tr>
                        <tr id="tr">
                        <th>Rank</th>
                          <th>Date</th>
                          <th>Patient's Name</th>
                          <th>Points Given</th>
                        </tr>`;   
            for(var i=0; i<data[1].length; i++){
                var date = data[1][i].created_at.date;
                var dt_ld= new Date(date);
                var options = { year: 'numeric', month: 'long', day: 'numeric' };
                text1 +=`<tr>
                    <td id="td">`+data[1][i].rank+`</td>
                       <td id="td">`+dt_ld.toLocaleDateString("en-US", options)+`</td>
                        <td id="td">`+data[1][i].patientname+`</td>
                        <td id="td">`+data[1][i].points+`%</td>
                        </tr>`;
            }$('#longestDuration').html(text1);
            }else{
              $('#longestDuration').html("No Patient/s under this criteria");
            }

            if(data[2] != 0){
            text2 +=   `<tr>
                          <th colspan="4"><center>Patient's with Highest Need</center></th>
                        </tr>
                        <tr id="tr">
                        <th>Rank</th>
                          <th>Patient's Name</th>
                          <th>Remaining Need</th>
                          <th>Points Given</th>
                        </tr>`;   
            for(var i=0; i<data[2].length; i++){
                text2 +=`<tr>
                      <td id="td">`+data[2][i].rank+`</td>
                        <td id="td">`+data[2][i].patientname+`</td>
                        <td id="td">`+data[2][i].lacking+`</td>
                        <td id="td">`+data[2][i].points+`%</td>
                        </tr>`;
            }$('#highestNeed').html(text2);
            }else{
              $('#highestNeed').html("No Patient/s under this criteria");
            }

            if(data[3] != 0){
            text3 +=   `<tr>
                          <th colspan="4"><center>Patient's Condition</center></th>
                        </tr>
                        <tr id="tr">
                        <th>Rank</th>
                          <th>Patient's Name</th>
                          <th>Critical Level</th>
                          <th>Points Given</th>
                        </tr>`;   
            for(var i=0; i<data[3].length; i++){
                text3 +=`<tr>
                      <td id="td">`+data[3][i].rank+`</td>
                        <td id="td">`+data[3][i].patientname+`</td>
                        <td id="td">`+data[3][i].condition+`</td>
                        <td id="td">`+data[3][i].points+`%</td>
                        </tr>`;
            }$('#condition').html(text3);
            }else{
              $('#condition').html("No Patient/s under this criteria");
            }

            sum_up +=   `<tr>
                          <th colspan="8"><center>Summary of Computations</center></th>
                        </tr>
                        <tr id="tr">
                        <th>Rank</th>
                          <th>Date</th>
                          <th>Patient's Name</th>
                          <th>Longest Duration</th>
                          <th>Number Donated</th>
                          <th>Total Need</th>
                          <th>Critical Level</th>
                          <th>Total Points</th>
                        </tr>`;   
            for(var i=0; i<data[4].length; i++){

                var date = data[4][i].created_at.date;
                var dt_f= new Date(date);
                var options = { year: 'numeric', month: 'long', day: 'numeric' };
                sum_up +=`<tr>
                        <td id="td">`+data[4][i].rank+`</td>
                        <td id="td">`+dt_f.toLocaleDateString("en-US", options)+`</td>
                        <td id="td">`+data[4][i].patientname+`</td>
                        <td id="td">`+data[4][i].c_points+`</td>`;
                        if(data[4][i].count != null){
                          console.log(data[4][i])
                          sum_up += `<td id="td">`+data[4][i].count+`</td>`; 
                        }
                        else{
                          sum_up +=`<td id="td">--</td>`;
                        }
                        
                        
             sum_up+=`           <td id="td">`+data[4][i].lacking+`</td>
                        <td id="td">`+data[4][i].condition+`</td>
                        <td id="td">`+data[4][i].sum+`%</td>
                        </tr>`;
            }$('#final').html(sum_up);




                  $.ajax({
                        type: "GET",
                        url: "/rcmPatient",
                        success:function(data){
console.log('success2');
                          console.log(data[0].voucher.length);
                            if(data.length == 0){
                              $('#noresult').html("NO RESULT");
                            }else{
                              $('#noresult').html(" ");
                            }
                            to+=`<table id="pdetails">
                              <tr id="tr">
                                  <th>DATE</th>
                                
                                  <th>PATIENT'S NAME</th>
                                  <th>VOUCHER AMOUNT</th>
                                  <th></th>
                                </tr>`;
                            for(var i=0; i<data.length; i++){
                                var date = data[i].created_at;
                                var dt = new Date(date);
                                var options = { year: 'numeric', month: 'long', day: 'numeric' };
                                to+=`
                                <tr>
                                      <td id="td">`+dt.toLocaleDateString("en-US", options)+`</td>
                                      <td>`+data[i].patientname+`</td>
                                      <td>`+data[i].total+`</td>
                                      <td>
<button class="btn-info" data-toggle ="modal" data-target="#p`+data[i].patientid+`">Sponsors</button>
                                      </td>
                                </tr>`;



                               console.log("success");
                            }to+=`</table>`;
  for(var i=0; i<data.length; i++){
 to+=`     <div class="modal fade" id="p`+data[i].patientid+`" role="dialog">
    <div class="modal-dialog" id="container" style="width: 550px; margin-top: 40px;">    
      <div class="modal-content" style="background-color: #ffeaa7">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <center><h4 class="modal-title" style="color: black; text-decoration: underline orange; font-weight: bold">SPONSORS</h4></center>
        </div>
        <div class="modal-body" id="det">
        <center>
          <table width="100%">
            <tr id="tr">
              <th>Date</th>
              <th>Donor's Name</th>
              <th>Voucher Value</th>
            </tr>`;
for(var j=0; j<data[i].voucher.length; j++){
  var dates = data[i].voucher[j].date;
  var dts = new Date(dates);
  var option = { year: 'numeric', month: 'long', day: 'numeric' };
  to+=`<tr>
          <td>`+dt.toLocaleDateString("en-US", options)+`</td>
          <td id="td">`+data[i].voucher[j].name+`</td>
          <td>`+data[i].voucher[j].voucherValue+`</td>
      </tr>`;
}
            
            
          to+=`</table>
        </center>
        </div>
      </div>     
    </div>
  </div>
                                `;
  }
                            $('#pntdetails').html(to);
                                  $.ajax({
                                      type: 'GET',
                                      url: '/helpVoucher',
                                      success:function(data){
                                        console.log('success3');
                                        console.log("done");
                                          if(data.length == 0){
                                              $('#x').html("All vouchers donated!");
                                          }
                                          for(var i=0; i<data.length; i++){
                                              var image =  data[i].count+'&nbsp;&nbsp;'+'<img src="'+data[i].value+'" width = 300/>'+'&nbsp;&nbsp;';
                                              $('#x').html(image);
                                          }
                                      },
                                      error:function(){
                                        console.log('error1');
                                      }
                                  });
                        },
                        error:function(){
                          console.log('error2');
                        }
                  });
                }else{
                    $('#x').html("No Voucher/s Donated");
                }
            },
            error:function(){
              console.log('error3');
            }
        });

};
</script>


@endsection
