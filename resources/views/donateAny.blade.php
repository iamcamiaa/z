<?php 
use App\Sponsor;
$get = Sponsor::where('userid', Auth::id())->where('status',null)->sum('voucherValue');
?>

@extends('layouts.raise')
@section('content')


<head>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<link rel="stylesheet" href="/css/form-basic.css">
<style>
    body {
        background-image: url('/images/bgg.png');
        top right no-repeat; 
        background-attachment:fixed;
        background-size: cover;
        margin-top: 0px;
    }
    #x{
        font-size: 40px;
        color: black;
    }
</style>

</head>

<br><br>

    
      

    <div class="main-content">
<center><p id="x"></p></center>
        <!-- You only need this form and the form-register.css -->

        <form style="background-color: #f9f9f9;" class="form-basic" action="{{url('/homepage')}}" method="post">

             {{csrf_field()}}

                    <div class="form-title-row">

       
          
@if(Sponsor::where('userid', Auth::id())->where('status', null)->get()->count() == 0)
    <div class="alert alert-info">
        You do not have vouchers in your account. Please avail vouchers first and try again later!
    </div>
@endif
                        <h1>Donate to HELPXP</h1>
                        <p style="font-style: normal; font-family: Arial Narrow; font-size: 11pt">Make an online donation to help our organization provide treatment, care and support to vulnerable children. Fill out the form below to send your donation. Thank you for your help!</p>
                    </div>

                   
                    <p style="color: white; 
                                    font-size: 20pt; 
                                    border-radius: 5px; 
                                    border: 2px solid orange; 
                                    background-color: orange;
                                    margin-top: -30px;  
                                    " id="total">
                            </p>

                        <!-- angel -->    

                    <div class="form-row">
                        <center><label>
                            Amount to be donated:
                            <input type="range" id="myRange" onchange="myFunction()" min="100" max="{{$get}}" step="100" name="amount">&nbsp;<p id="demo"></p>
                        </label></center>
                    </div>
        
<script>
function myFunction() {
    var x = document.getElementById("myRange").value;
    document.getElementById("demo").innerHTML = x;
}
</script>

       <!-- end -->

                    <div class="form-row" style="margin-left: 35px">
                        <button type="submit" name="submit">Donate</button>
                    </div>

                



        </form>

    </div>

<div class="modal fade" id="success" role="dialog">
    <div class="modal-dialog" id="container" style="width:0 auto; padding: 10px;">    
      <div class="modal-content" style="background-color: #a4b0be; border-radius: 0px;">
        <div class="modal-body">
          <center><h3 id="msg" style="color: white; text-decoration: underline orange;"></h3>
          <br>
          <button type="button" class="btn btn-default" data-dismiss="modal" style="background-color: orange; font-weight: bold; color: white;">Okay</button></center>
          </div>
      </div>     
    </div>
  </div>



@if(Session::has('success'))
<script>
       var change = "";
$(document).ready(function() {
    change = `<h3 id="msg" style="color: white; text-decoration: underline orange;">Successful Donation!</h3>`;
    $('#msg').html(change);
    $('#success').modal('show');
});
    </script>
    <?php Session::forget('success'); ?> 


@elseif(Session::has('info'))
    <div class="alert alert-confirm">
    <script>
        confirm('You cannot donate your desired amount. However you can donate {{ Session::get('avblVoucher', '') }} worth of voucher/s.');
    </script>  
</div>

<script>
       var change = "";
$(document).ready(function() {
    change = `<h3 id="msg" style="color: white; text-decoration: underline orange;">You cannot donate your desired amount. However you can donate `+ Session::get('avblVoucher', '') +` worth of voucher/s.</h3>`;
    $('#msg').html(change);
    $('#success').modal('show');
});
    </script>
    <?php Session::forget('info'); ?> 

@endif


</div>
            </div>
        </div>
    </div>
</div>

<!-- <script>
function check(){
    var voucher = document.getElementById("voucher").value;
    var gl = document.getElementById("gl").value;
    var goal = document.getElementById("goal").value;

if(gl > voucher){
    alert("greater");
}
     // if(gl < 200)
     //     alert("minimum of 200");

    //  if(voucher < gl)
    //  alert("Your donation is greater");
    // else
    //  alert("success");
    
}
</script>
 -->

<!-- <script>   
        alert("donation has a remaining balance");
</script> -->


    <script type="text/javascript">
     $(document).ready(function (){
      var to = "";
      var total = "";
      var div=$(this).parent();
        $.ajax({
            type: 'GET',
            url: '/getDen',
            data:{
                '_token': $('p[name=_token]').val()
            },
            success:function(data){

                for(var i=0; i<data.length; i++){

                    var image = data[i].count+'&nbsp;&nbsp;'+'<img src="'+data[i].value+'" width = 200/>'+'&nbsp;&nbsp;';
                    $('#x').append(image);
                } 
                total = data[data.length - 1].total+' TOTAL WORTH OF VOUCHERS';
                $('#total').append(total);
                console.log(data);
            },
            error:function(){
            }
        });
    });
</script>
         

<!-- <script>
function check(){
	var voucher = document.getElementById("voucher").value;
	var gl = document.getElementById("gl").value;
	var goal = document.getElementById("goal").value;

if(gl > voucher){
	alert("greater");
}
	 // if(gl < 200)
	 // 	alert("minimum of 200");

	//  if(voucher < gl)
	// 	alert("Your donation is greater");
	// else
	// 	alert("success");
	
}
</script>
 -->

<!-- <script>   
		alert("donation has a remaining balance");
</script> -->

<div id="fh5co-feature-product" class="fh5co-section-gray">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 text-center heading-section">
                        <h3 style="font-size: 23pt">On-going Stories.</h3>
                        <p style="color: #3d3d29">Make an online donation to help our organization provide treatment, care and support to vulnerable people.</p>
                    </div>
                </div>

                
                @foreach ($data as $patients)


<div style="box-sizing: border-box; float: left; width: 24%; padding: 8px; margin-left: 12px;  content: ""; clear: both;
    display: table;">
 <div class="row row-bottom-padded-md" >
  <div class="col-md-12">
    <ul id="fh5co-portfolio-list">

      <li style="background-image: url( {{ url('storage/picture/'.$patients->filename) }}); width: 100%">
                <a href="http://localhost:8000/list/{{$patients['patientid']}}/view" class="color-3">
                  <div class="case-studies-summary">
                    <span>Give Love</span>
                    @foreach($patients->stories as $story)
                    <h2>{{$story->storytitle}}</h2><br>
                    @endforeach
                    <div align="left"><input type="button" name="donate" value="+Read More" class="btn btn-primary-info"></div>
<br>
                    <div class="progress" style="width: 100%;margin-bottom: 0px">
    <div class="progress-bar" role="progressbar" aria-valuenow="{{$patients->TotalRedeem/$patients->goal*100}}" aria-valuemin="0" aria-valuemax="100" style="width:{{$patients->TotalRedeem/$patients->goal*100}}%">
    </div>
  </div>
  <br>
                <p style="text-align:center;font-size: 20px; color: #fff">P{{number_format($patients->TotalRedeem)}} raised of P{{number_format($patients->goal)}}</p>

                  </div>
                </a> 
            </li>

      </ul>
    </div>
  </div>
</div>

@endforeach  


                </div>
            </div>

@endsection

