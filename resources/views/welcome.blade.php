<?php 
use App\Sponsor;
$sponsor = Sponsor::where('userid', Auth::id())->get();

?>

@extends('layouts.welcome')
@section('content')

<head>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>

<div class="modal fade" id="success" role="dialog">
    <div class="modal-dialog" id="container" style="width:0 auto; padding: 10px;">    
      <div class="modal-content" style="background-color: #a4b0be; border-radius: 0px;">
        <div class="modal-body">
          <center><h4 id="msg" style="color: white; text-decoration: underline orange;"></h4>
          <br>
          <button type="button" class="btn btn-default" data-dismiss="modal" style="background-color: orange; font-weight: bold; color: white;">Okay</button></center>
          </div>
      </div>     
    </div>
  </div>
@if(Session::has('status'))
    <script>
    var change = "";
$(document).ready(function() {
    change = `<h3 id="msg" style="color: white; text-decoration: underline orange;">To confirm, please check your email!</h3>`;
    $('#msg').html(change);
    $('#success').modal('show');
});
</script><?php Session::forget('status'); ?>   
@elseif(Session::has('alert'))
    <script>
    var change = "";
$(document).ready(function() {
    change = `<h3 id="msg" style="color: white; text-decoration: underline orange;">Something went wrong. Please come back later.</h3>`;
    $('#msg').html(change);
    $('#success').modal('show');
});
</script><?php Session::forget('alert'); ?>   
@elseif(Session::has('success'))
    <script>
    var change = "";
$(document).ready(function() {
    change = `<h3 id="msg" style="color: white; text-decoration: underline orange;">Your account is fully activated!</h3>`;
    $('#msg').html(change);
    $('#success').modal('show');
});
</script><?php Session::forget('success'); ?>   
@endif

<!-- start sa all time donations -->

  <!-- end sa all time donations -->



<!-- start div para sa stories -->

 @foreach ($data as $patients)
<div style="box-sizing: border-box; float: left; width: 25%; padding: 5px;  content: ""; clear: both;
    display: table;">
 <div class="row row-bottom-padded-md" >
  <div class="col-md-12">
    <ul id="fh5co-portfolio-list">

      <li style="background-image: url( {{ url('storage/picture/'.$patients->filename) }}); width: 100%">
                <a href="http://localhost:8000/list/{{$patients['patientid']}}/view" class="color-3">
                  <div class="case-studies-summary">
                    <span>Give Love</span>
                   <h2>{{$patients->stories[0]->storytitle}}</h2><br>
                    <div align="center"><input type="button" name="donate" value="Donate" class="btn btn-primary-info"></div>
                  </div>
                </a> 
            </li>

      </ul>
    </div>
  </div>
</div>


@endforeach  

@endsection