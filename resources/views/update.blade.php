@extends('layouts.patient')
@section('content')

<head>

    
    <link rel="stylesheet" href="/css/form-basic.css">


</head>

<br><br>

<style>
   body {
        background-image: url('/images/bgg.png');
        top right no-repeat; 
        background-attachment:fixed;
        background-size: cover;
        margin-top: 0px;

    }
</style>

    <div class="main-content" >

        <form class="form-basic" action="{{url('/saveUpdate')}}" method="post" enctype="multipart/form-data" style="background-color: #F9F9F9">
        {{csrf_field()}}

            <div class="form-title-row">
                <h1>What's the update?</h1>
            </div>
            <input type="hidden" name="patientid" value="{{$patient}}">
            <div class="form-row">
                <label>
                    <span>Update Title:</span>
                    <input type="text" name="updatetitle">
                </label>

            </div>

            <div class="form-row">
                <label>
                    <span>Post Story</span>
                    <textarea name="story" rows="4" cols="60"></textarea>
                </label>
            </div>

            <div class="form-row">
                <label>
                    <span>Add a profile goal</span>
                    <span style="padding-left: 0;"><input type="file" name="file[]" multiple></span>
                </label>

            </div>
            <div >
                <label>
                    <span style="font-color: white"><input type="submit" class="btn btn-primary" value="Submit">
                    <input style="color: white" type="reset" class="btn btn-danger" value="Reset"></span>
                </label>
            </div>

        </form>

    </div>
    <br><br>

</body>

</html>


@endsection