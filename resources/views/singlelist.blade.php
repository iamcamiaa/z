

@extends('layouts.single')
@section('content')


<style>
/* The Modal (background) */
.modal {
    display: none; /* Hidden by default */
    position: fixed; /* Stay in place */
    z-index: 1; /* Sit on top */
    padding-top: 100px; /* Location of the box */
    left: 0;
    top: 0;
    width: 100%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0.9); /* Black w/ opacity */
}

/* Modal Content (image) */
.modal-content {
    margin: auto;
    display: block;
    width: 100%;
    max-width: 700px;
}

/* Add Animation */
.modal-content, #caption {    
    -webkit-animation-name: zoom;
    -webkit-animation-duration: 0.6s;
    animation-name: zoom;
    animation-duration: 0.6s;
}

@-webkit-keyframes zoom {
    from {-webkit-transform:scale(0)} 
    to {-webkit-transform:scale(1)}
}

@keyframes zoom {
    from {transform:scale(0)} 
    to {transform:scale(1)}
}

/* 100% Image Width on Smaller Screens */
@media only screen and (max-width: 700px){
    .modal-content {
        width: 100%;
    }
}
</style>



<?php use App\stories; ?>
<div style="float: right; overflow: auto; margin-top: 25px; margin-left: -130px; margin-right: 150px; max-width: 400px; width: 400px">
	<div style="background-color: #FFF; padding: 20px; height: 300px; min-height: 300px; max-width: 400px; width: 400px">
	<strong style="color: #1C1C1C; font-size: 23pt; letter-spacing: 0.07em">&#8369; {{number_format($patient->TotalRedeem)}}</strong><span style="font-family: Palanquin; font-size: 17pt; color: #1C1C1C; letter-spacing: 0.1em"> out of  P {{number_format($patient->goal)}} goal</span>
	<div style="color: blue; font-style: italic">
		Amount lacking: {{number_format($patient->goal - $patient->TotalRedeem)}}
	</div><br><br>

	<div class="progress">
    <div class="progress-bar" role="progressbar" aria-valuenow="{{$patient->TotalRedeem/$patient->goal*100}}" aria-valuemin="0" aria-valuemax="100" style="width:{{$patient->TotalRedeem/$patient->goal*70}}%">
    </div>
  </div>
 
	<div align="center">
	@if($patient['goal'] == $patient['TotalRedeem'])
	<span><a disabled href="http://localhost:8000/sponsorDonate/{{ $patient['patientid'] }}" class="btn btn-primary">Donate Now</a></span><br><br>
	@else
	<span><a href="http://localhost:8000/sponsorDonate/{{ $patient['patientid'] }}" class="btn btn-primary">Donate Now</a></span><br><br>
	@endif
	@if(Auth::id() == $patient['userid'])
	<a href="/update/{{$patient['patientid']}}" class="btn btn-success">UPDATE STORY</a>
	&nbsp;&nbsp;&nbsp;
	<a href="http://localhost:8000/confirm/{{ $patient['patientid'] }}" class="btn btn-info">REDEEM</a>
	@endif

	</div><br><br></div>
	<h4 style="font-weight: bold; color: grey; margin-bottom: 2px; padding: 5px; letter-spacing: 0.1em">Proof:</h4>
	
	

	@foreach($pic as $ap)
		<h4><img id="img{{$ap->filename}}" src="{{  url('storage/picture/'.$ap->filename)}}" width="400px" height="450px" />
		<div id="m{{$ap->filename}}" class="modal">
  			<span style="float: right; margin-top: -90px; font-size: 100px; cursor: pointer;" class="c{{$ap->filename}}">&times;</span>
  			<img class="modal-content" id="mimg{{$ap->filename}}">
		</div>
	@endforeach
	<center>{{$pic->links()}}</center>
</h4>
</div>

<!-- start sa story title -->
<br>
<div style="padding-left: 150px; background-color: #fff; max-width: 800px; margin-left: 150px; padding-left: 6px; padding-right: 0;padding-top: 30px; overflow: auto; float: left;">
 <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
@if($patient['status'] == "pending")
		<center><div class="alert alert-info">
        <strong>This Story will not be published not until the Administrator will confirmed it.</strong>
    	</div></center>
@endif            
<div>
	
	<img style="max-width: 800px; max-height: 500px" src="{{  url('storage/picture/'.$patient->filename)}}" width="800px" height="500px" />
</div><br>

<h2 style="font-weight: bold; letter-spacing: 0.1em; color: #232323">{{$story['storytitle']}} </h2>

<!-- end sa story title -->



<div class="containerrr">
@if(stories::where('patientid', $patient->patientid)->where('role', null)->count() != 0)
<a style="margin-left: 150px;" class="btn btn-info" href="http://localhost:8000/update/{{$story['patientid']}}/view">View Updates</a></div><br>
@else
<a disabled style="margin-left: 120px;" class="btn btn-info" href="#">No updates available</a></div><br>
@endif


<a href="http://localhost:8000/donors/{{$story['patientid']}}/view" class="btn btn-info" style="margin-left: 360px">Donors</a>


<!-- start tabbing -->
<h4 style="letter-spacing: 0.1em; font-weight: bold; color: grey; margin-bottom: 2px; padding: 5px">Story

	<div style="background-color: #F0EFEF; height: 200px; max-width: 800px; width: 800px;">
		<h4 style="margin-top: 5px; color: black; letter-spacing: 0.1em; padding: 15px">{{$story['story']}}</h4>
	</div>
 
<div>
	<h4 style="letter-spacing: 0.1em; font-weight: bold; color: grey; margin-bottom: 2px; padding: 5px">Information</h4>
	
	<h4 style="margin-top: 5px; font-size: 12pt; color: black; letter-spacing: 0.1em; padding: 15px">
	Beneficiary Name: <strong>{{$patient->patientname}}</strong><br><br>
	Contact Info: <strong><pre style="font-size: 14pt; width: 800px">+63{{$patient->userName->contact}}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$patient->userName->email}}</pre></strong><br>
	Illness: <strong>{{$patient['illness']}}</strong></h4>
	</div>
</div> <!-- end tabbing -->
	

<!-- start sa update -->


<!-- update end -->

	</div>	
</div>
</div>


<script>

@foreach($pic as $ap)

// Get the modal
var modal = document.getElementById('m{{$ap->filename}}');

// Get the image and insert it inside the modal - use its "alt" text as a caption
var img = document.getElementById('img{{$ap->filename}}');
var modalImg = document.getElementById('mimg{{$ap->filename}}');
img.onclick = function(){
    modal.style.display = "block";
    modalImg.src = this.src;
}


// Get the <span> element that closes the modal
var span = document.getElementsByClassName("c{{$ap->filename}}")[0];

// When the user clicks on <span> (x), close the modal
span.onclick = function() { 
    modal.style.display = "none";
}

@endforeach

</script>


@endsection


