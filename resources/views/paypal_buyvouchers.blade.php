

@extends('layouts.voucher')

 
@section('content')


<head>
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <!-- <link rel="stylesheet" href="/css/form-basic.css"> -->
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <style>
    body {

        background-image: url('/images/bgg.png');
        top right no-repeat; 
        background-attachment:fixed;
        background-size: cover;
        margin-top: 0px;

    }
        h1 {
        display: inline-block;
        box-sizing: border-box;
        color:  #4c565e;
        font-size: 24px;
        padding: 0 10px 15px;
        border-bottom: 2px solid #6caee0;
        margin: 0;}
    </style>

</head>


<br><br>

        <div style="" align="center">
                <form class="form-basic" action="{{url('/voucherpaypal')}}" method="post">
            {{csrf_field()}}
        
            <div class="form-title-row">
                <h1 style="font-size: 45px;font-family: Verdana; margin-left: 50px;">Buy Vouchers<img src="/images/arrows.png" height="10%" width="15%" /></h1>
            </div><br><br>


            <div class="form-row">
                <label>
                   <!--  <p style="text-align: center;"><input type="checkbox" name="check100" value="100">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -->
                    <img src="/images/v_100.JPG" height="28%" width="90%"/></p>
                    
            
            <p style="text-align: center">

           

            <input type=number min="0" name='qty100' id='qty100' style="width: 100px"  onclick="getTotal()" />
          
            </p>


                </label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            

                <label>
                    <!--  <p style="text-align: center;"><input type="checkbox" name="check500" value="500">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  -->            
                    <img src="/images/v_500.JPG" height="28%" width="90%" /></p>
            
            <p style="text-align: center">
            
            <input type='number' min="0" name='qty500' id='qty500' style="width: 100px" onclick="getTotal()" />
            
            </p>

                </label>
            </div><br><br>

             <div id="totalVoucher" style="float: center;width:500px;height:100px; font-size: 20pt;">
             </div><br>

            <div class="form-row">
                <label>
                     <!-- <p style="text-align: center;"><input type="checkbox" name="check1000" value="1000">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -->
                     <img src="/images/v_1000.JPG" height="28%" width="90%" /></p>
            
            <p style="text-align: center">
            
            <input type='number' min="0" name='qty1000' id='qty1000' style="width: 100px" onclick="getTotal()" />
            
            </p>

                </label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
           

            
                <label>
                    <!-- <p style="text-align: center;"><input type="checkbox" name="check5000" value="5000">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -->
                    <img src="/images/v_5000.JPG" height="28%" width="90%" /></p>
            
            <p style="text-align: center">
            
            <input type='number' min="0" name='qty5000' id='qty5000' style="width: 100px" onclick="getTotal()" />
            
            </p>

                </label>
            </div>
<br><br>


<br>
     
    
    


             
    
<br>
            <div >
                <label>
                    <span style="font-color: white"><input type="submit" class="btn btn-primary" value="Submit">
                    <input type="reset" class="btn btn-danger" value="Reset"></span>
                </label>
            </div>
              
        </form>

        </div>

</div>

<div class="modal fade" id="success" role="dialog">
    <div class="modal-dialog" id="container" style="width:0 auto; padding: 10px;">    
      <div class="modal-content" style="background-color: #a4b0be; border-radius: 0px;">
        <div class="modal-body">
          <center><h3 id="msg" style="color: white; text-decoration: underline orange;"></h3>
          <br>
          <button type="button" class="btn btn-default" data-dismiss="modal" style="background-color: orange; font-weight: bold; color: white;">Okay</button></center>
          </div>
      </div>     
    </div>
  </div>
@if(Session::has('error'))
    <script>
var change = "";
$(document).ready(function() {
    change = `<h3 id="msg" style="color: white; text-decoration: underline orange;">Cant buy with that quantity!</h3>`;
    $('#msg').html(change);
    $('#success').modal('show');
});
    </script>
@elseif(Session::has('success'))
    <script>
var change = "";
$(document).ready(function() {
    change = `<h3 id="msg" style="color: white; text-decoration: underline orange;">Thankyou for purchasing!</h3>`;
    $('#msg').html(change);
    $('#success').modal('show');
});
    </script>
@elseif(Session::has('notenough'))
    <script>
var change = "";
$(document).ready(function() {
    change = `<h3 id="msg" style="color: white; text-decoration: underline orange;">You dont have enough balance in your paypal account!</h3>`;
    $('#msg').html(change);
    $('#success').modal('show');
});
    </script>
@elseif(Session::has('nonzero'))
    <script>
var change = "";
$(document).ready(function() {
    change = `<h3 id="msg" style="color: white; text-decoration: underline orange;">No voucher purchase!</h3>`;
    $('#msg').html(change);
    $('#success').modal('show');
});
    </script>
@endif


@if($message = Session::get('success_pay'))
<script>
var change = "";
$(document).ready(function() {
    change = `<h3 id="msg" style="color: white; text-decoration: underline orange;">Payment Successful!</h3>`;
    $('#msg').html(change);
    $('#success').modal('show');
});
</script>
<?php Session::forget('success_pay'); ?> 
@endif

@if($message = Session::get('error_pay'))
<script>
var change = "";
$(document).ready(function() {
    change = `<h3 id="msg" style="color: white; text-decoration: underline orange;">Payment Failed!</h3>`;
    $('#msg').html(change);
    $('#success').modal('show');
});
</script>
<?php Session::forget('error_pay'); ?> 
@endif
<script>
     
function getTotal() {
    var v1 = document.getElementById("qty100").value;
    var v2 = document.getElementById('qty500').value;
    var v3 = document.getElementById('qty1000').value;
    var v4 = document.getElementById('qty5000').value;

    var total = (v1 * 100) + (v2 * 500) + (v3 * 1000) + (v4 * 5000);
    document.getElementById("totalVoucher").innerHTML =  "Total Vouchers &#8369 "+ total;
}
</script>


<br><br>
@endsection



