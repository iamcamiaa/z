<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>HELPXP - Dashboard</title>
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/font-awesome.min.css" rel="stylesheet">
	<link href="css/datepicker3.css" rel="stylesheet">
	<link href="css/styles.css" rel="stylesheet">
	
	<!--Custom Font-->
	<link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">
	<!--[if lt IE 9]>
	<script src="js/html5shiv.js"></script>
	<script src="js/respond.min.js"></script>
	<![endif]-->

<style>
/* The Modal (background) */
.modal {
    display: none; /* Hidden by default */
    position: fixed; /* Stay in place */
    z-index: 1; /* Sit on top */
    padding-top: 100px; /* Location of the box */
    left: 0;
    top: 0;
    width: 100%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0.9); /* Black w/ opacity */
}

/* Modal Content (image) */
.modal-content {
    margin: auto;
    display: block;
    width: 80%;
    max-width: 700px;
}

/* Add Animation */
.modal-content, #caption {    
    -webkit-animation-name: zoom;
    -webkit-animation-duration: 0.6s;
    animation-name: zoom;
    animation-duration: 0.6s;
}

@-webkit-keyframes zoom {
    from {-webkit-transform:scale(0)} 
    to {-webkit-transform:scale(1)}
}

@keyframes zoom {
    from {transform:scale(0)} 
    to {transform:scale(1)}
}

/* 100% Image Width on Smaller Screens */
@media only screen and (max-width: 700px){
    .modal-content {
        width: 100%;
    }
}
</style>

</head>
<body>
	<nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#sidebar-collapse"><span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span></button>
				<a class="navbar-brand" href="{{ url('/displayusers') }}"><span>Help</span>XP</a>
			</div>
		</div><!-- /.container-fluid -->
	</nav>
	<div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
		<div class="profile-sidebar">
			<div class="profile-userpic">
				<img src="http://placehold.it/50/30a5ff/fff" class="img-responsive" alt="">
			</div>
			<div class="profile-usertitle">
				<div class="profile-usertitle-name">{{ Auth::user()->username }}</div>
				<div class="profile-usertitle-status"><span class="indicator label-success"></span>Online</div>
			</div>
			<div class="clear"></div>
		</div>
		<div class="divider"></div>
		<!-- <form role="search">
			<div class="form-group">
				<input type="text" class="form-control" placeholder="Search">
			</div>
		</form> -->
		<ul class="nav menu">
			<li><a href="{{url('/displayusers')}}"><em class="fa fa-dashboard">&nbsp;</em> Dashboard</a></li>
			<li class="active"><a href="{{ url('/displaypatients') }}"><em class="fa fa-calendar">&nbsp;</em> Patients</a></li>
			<li><a href="{{ url('/displaysponsors') }}"><em class="fa fa-bar-chart">&nbsp;</em> Sponsors</a></li>
			<li><a class="fa fa-power-off" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">&nbsp;&nbsp;Logout
                                    </a>
                                    
                                  
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
            </li>
		</ul>
	</div><!--/.sidebar-->
		
	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="#">
					<em class="fa fa-home"></em>
				</a></li>
				<li class="active">Dashboard</li>
			</ol>
		</div><!--/.row-->
		
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Patients</h1>
			</div>
		</div><!--/.row-->
	

	<div class="panel panel-container">
	<div class="row">
	<table class="table">
	
	<th class="fixed-table-container">Name</th>
	<th class="fixed-table-container">Goal</th>
	<th class="fixed-table-container" colspan="5">Requirements</th>
	<th class="fixed-table-container"></th>
	
	<!-- <th>Illness</th>
	<th>Story</th>
	<th>Amount</th> --><br>



<p id="patient">


	@foreach ($approve as $apr)
		<tr class="fixed-table-container">
			<td class="fixed-table-container">{{$apr->patient->userName->fname}} {{$apr->patient->userName->lname}}</td>
			<td class="fixed-table-container">{{$apr->patient->goal}}</td>
		@foreach($apr->picture as $ap)
			<td class="fixed-table-container"><center><img id="img{{$ap->filename}}" src="{{  url('storage/picture/'.$ap->filename) }}" width="100px" height="100px" /></center></td>
			<div id="m{{$ap->filename}}" class="modal">
  				<span style="float: right; margin-top: -90px; font-size: 100px; cursor: pointer;" class="c{{$ap->filename}}">&times;</span>
  				<img class="modal-content" id="mimg{{$ap->filename}}">
			</div>
		@endforeach


<!-- The Modal -->


<form action="{{url('approved')}}" method="post">
{{csrf_field()}}
			<td class="fixed-table-container" align="center">
				<button id="confirm" class="btn btn-danger">Confirm</button>
			</td>
			<input type="hidden" name="patientid" value="{{$apr->patient->patientid}}">
</form>


<script>
	document.getElementById('confirm').onclick = function(){
		document.getElementById('confirm').innerHTML = "Confirmed";
	} 
</script>


<script>

@foreach ($approve as $apr)
@foreach($apr->picture as $ap)

// Get the modal
var modal = document.getElementById('m{{$ap->filename}}');

// Get the image and insert it inside the modal - use its "alt" text as a caption
var img = document.getElementById('img{{$ap->filename}}');
var modalImg = document.getElementById('mimg{{$ap->filename}}');
img.onclick = function(){
    modal.style.display = "block";
    modalImg.src = this.src;
}


// Get the <span> element that closes the modal
var span = document.getElementsByClassName("c{{$ap->filename}}")[0];

// When the user clicks on <span> (x), close the modal
span.onclick = function() { 
    modal.style.display = "none";
}

@endforeach
@endforeach
</script>





















</tr>
		
@endforeach



</p>
</table><br><br>


				
			</div><!--/.row-->
		</div>
		
			</div><!--/.col-->
			
		</div><!--/.row-->
	</div>	<!--/.main-->
	
	<script src="js/jquery-1.11.1.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/chart.min.js"></script>
	<script src="js/chart-data.js"></script>
	<script src="js/easypiechart.js"></script>
	<script src="js/easypiechart-data.js"></script>
	<script src="js/bootstrap-datepicker.js"></script>
	<script src="js/custom.js"></script>
	<script>
		window.onload = function () {
	var chart1 = document.getElementById("line-chart").getContext("2d");
	window.myLine = new Chart(chart1).Line(lineChartData, {
	responsive: true,
	scaleLineColor: "rgba(0,0,0,.2)",
	scaleGridLineColor: "rgba(0,0,0,.05)",
	scaleFontColor: "#c5c7cc"
	});
};
	</script>
		




