<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class patientNotification extends Notification
{
    //patient notif / complete na iyang goal
    use Queueable;
    protected $patient_goal;
    
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($patient_goal)
    {
        $this->patient_goal = $patient_goal;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database', 'broadcast'];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toDatabase($notifiable)
    {
        return [
            'patient_goal' => $this->patient_goal,
        ];
    }

    public function toBroadcast($notifiable)
    {
        return [
            'patient_goal' => $this->patient_goal,
        ];
    }
}
