<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class patientRedeemed extends Notification
{
    //admin / naay patient na ning redeem
    use Queueable;
    protected $patientRedeemed;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($patientRedeemed)
    {
        $this->patientRedeemed = $patientRedeemed;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database', 'broadcast'];
    }


    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toDatabase($notifiable)
    {
        return [
            'patientRedeemed' =>$this->patientRedeemed,
        ];
    }

    public function toBroadcast($notifiable)
    {
        return [
            'patientRedeemed' =>$this->patientRedeemed,
        ];
    }
}
