<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class buyVoucher extends Notification
{
    //admin notification / confirm sa pag palit sa voucher
    use Queueable;
    protected $buyVoucher;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($buyVoucher)
    {
        $this->buyVoucher = $buyVoucher;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database', 'broadcast'];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toDatabase($notifiable)
    {
        return [
            'buyVoucher' => $this->buyVoucher,
        ];
    }

    public function toBroadcast($notifiable)
    {
        return [
            'buyVoucher' => $this->buyVoucher,
        ];
    }
}
