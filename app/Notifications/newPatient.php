<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class newPatient extends Notification
{
    //bag ong apply as patient / admin notification
    use Queueable;
    protected $newPatient;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($newPatient)
    {
        $this->newPatient = $newPatient;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database', 'broadcast'];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toDatabase($notifiable)
    {
        return [
            'newPatient' =>$this->newPatient,
        ];
    }

    public function toBroadcast($notifiable)
    {
        return [
            'newPatient' =>$this->newPatient,
        ];
    }
}
