<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\BroadcastlMessage;

class patientConfirm extends Notification
{
    //patient notif / approved story
    use Queueable;
    protected $patientConfirm;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($patientConfirm)
    {
        $this->patientConfirm = $patientConfirm;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database', 'broadcast'];
    }


    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toDatabase($notifiable)
    {
        return [
            'patientConfirm' => $this->patientConfirm,
        ];
    }

    public function toBroadcast($notifiable)
    {
        return [
            'data' => [
                'patientConfirm' => $this->patientConfirm,
            ]
        ];
    }
}
