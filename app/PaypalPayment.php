<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaypalPayment extends Model
{
    public function user(){
        return $this->hasOne('App\User', 'userid', 'id');
    }
}
