<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;

use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;
use Paypal\Auth\OAuthTokenCredential;
use Paypal\Rest\ApiContext;
use Redirect;
use Session;
use URL;
use Auth;
use App\PaypalPayment;
use App\User;

class PaymentController extends Controller
{
    private $_api_context;
    public function __construct(){
    	//paypal api context
    	$paypal_conf = \Config::get('paypal');
    	$this->_api_context = new \PayPal\Rest\ApiContext(new \PayPal\Auth\OAuthTokenCredential(
    		$paypal_conf['client_id'],
    		$paypal_conf['secret'])
    	);
    	$this->_api_context->setConfig($paypal_conf['settings']);
    }

    public function paywithpaypal(Request $request){
    	$payer = new Payer();
		$payer->setPaymentMethod("paypal");

		$item1 = new Item();
		$item1->setName('Item 1')
    		->setCurrency('PHP')
    		->setQuantity(1)
    		->setPrice($request->get('amount'));

		$itemList = new ItemList();
		$itemList->setItems(array($item1));

		$amount = new Amount();
		$amount->setCurrency("PHP")
    		->setTotal($request->get('amount'));

    	$transaction = new Transaction();
		$transaction->setAmount($amount)
    		->setItemList($itemList)
    		->setDescription("Payment description")
    		->setInvoiceNumber(uniqid());

    	$redirect_urls = new RedirectUrls();
    	$redirect_urls->setReturnUrl(URL::to('status'))
    		->setCancelUrl(URL::to('status')); 


    	$payment = new Payment();
		$payment->setIntent("sale")
    		->setPayer($payer)
    		->setRedirectUrls($redirect_urls)
    		->setTransactions(array($transaction));

    	// $request = clone $payment;

    	try {
    		$payment->create($this->_api_context);
		} catch (\Paypal\Exception\PPConnectionException $ex) {
			 if(\Config::get('app.debug')){
			 	\Session::put('error', 'Connection timeout');
			 	return Redirect::to('/paypal');
			 }else{
			 	\Session::put('error', 'Some error occur, sorry for inconvenient');
			 	return Redirect::to('/paypal');
			 }
		}

		foreach($payment->getLinks() as $link){
			if($link->getRel() == 'approval_url'){
				$redirect_url = $link->getHref();
				break;
			}
		}

		//add paymentid to session
        \Session::put('id', Auth::id());
        \Session::put('amount', $request->get('amount'));
		\Session::put('paypal_payment_id', $payment->getId());

		if(isset($redirect_url)){
			//redirect to paypal
			return Redirect::away($redirect_url);
		}

		\Session::put('error', 'Unknown Error Occured');
		return Redirect::to('/paypal');
    }

    public function paymentstatus(){

        $new_payment = new PaypalPayment();
        $new_payment->paypal_id = Session::get('paypal_payment_id');
        $new_payment->userid = Session::get('id');
        $new_payment->amount = Session::get('amount');
        $new_payment->save();

        $user = User::findOrFail(Session::get('id'));
        $user->paypal_balance = $user->paypal_balance + Session::get('amount');
        $user->save();

    	$payment_id = Session::get('paypal_payment_id');
    	Session::forget('paypal_payment_id');

    	if(empty(Input::get('PayerID'))|| empty(Input::get('token'))){
    		\Session::put('error_pay', true);
    		return Redirect::to('/paypal');
    	}

    	$payment = Payment::get($payment_id, $this->_api_context);
    	$execution = new PaymentExecution();
    	$execution->setPayerId(Input::get('PayerID'));
     

    	//execute payment
    	$result = $payment->execute($execution, $this->_api_context);

    	if($result->getState() == 'approved'){
    		\Session::put('success_pay', true);
    		return Redirect::to('/buyvoucherpaypal');
    	}

    	\Session::put('error_pay', true);
    	return Redirect::to('/paypal');
    }
}
















