<?php

namespace App\Http\Controllers\Auth;
use GuzzleHttp\Client as Guzzle;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

use Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;


    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */

    // protected function age(){
    //     return Validator::extend('olderThan', function($attribute, $value, $parameters)
    //     {
    //         $minAge = ( ! empty($parameters)) ? (int) $parameters[0] : 18;
    //         return (new DateTime)->diff(new DateTime($value))->y >= $minAge;

    // // or the same using Carbon:
    // // return Carbon\Carbon::now()->diff(new Carbon\Carbon($value))->y >= $minAge;
    //     });
    // }

    



    protected function validator(array $data)
    {    
        return Validator::make($data, [
            'fname' => 'required|string|max:255',
            'lname' => 'required|string|max:255',
            'username' => 'required|string|max:255|unique:users',
            'email' => 'required|string|email|max:255',
            'contact' => 'required|regex:/^[0-9]+$/|min: 11|max:13',
            'password' => 'required|string|min:6|confirmed',
            'birthdate' => 'required|date|before:18 years ago',
        ]);
    }

   

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */


    protected function create(array $data)
    {   
        return User::create([
            'fname' => ucfirst($data['fname']),
            'lname' => ucfirst($data['lname']),
            'email' => $data['email'],
            'address' => $data['address'],
            'birthdate' => $data['birthdate'],
            'username' => $data['username'],
            'contact' => $data['contact'],
            'password' => Hash::make($data['password']),
        ]);  
    }

    protected function register(Request $request){
        $input = $request->all();

            $data = $this->create($input)->toArray();
            $data['token'] = str_random(25);

            $user = User::find($data['id']);
            $user->token = $data['token'];
            $user->save();

            Mail::send('confirmation', $data, function($msg) use($data){
                $msg->to($data['email']);
                $msg->subject('Registration Confirmation');
            });
            return redirect('/')->with('info', true)->with('status', 'pls');
        
      
    }
    public function confirmation($token){
         $user = User::where('token', $token)->first();

        if(!is_null($user)){
            $user->confirmed = 1;
            $user->token = '';
            $user->save();
            return redirect('/')->with('success', true);
        }
        return redirect('/')->with('alert', true);
    }
} 
