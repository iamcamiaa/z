<?php

namespace App\Http\Controllers;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use App\Sponsor;
use App\User;
use App\Donation;
use App\Patient;
use App\Stories;
use App\Picture;
use DB;
use Auth;
use App\Notifications;

class UserController extends Controller
{
    public function history(){
//sponsors                      
$sponsor = Sponsor::where('userid', Auth::id())->get();
$donation = Donation::where('patientid', '!=', null)->get();
$sponsorCollect = new Collection();
foreach($sponsor as $spr){
    foreach($donation as $dnr){
        if($dnr['sponsor_serial'] == $spr['sponsor_serial']){
            $sponsorCollect->push($spr);
            }
        }
}

// $helpxp = DB::table('donations')
// ->join('sponsors', 'sponsors.sponsor_serial', 'donations.sponsor_serial')
// ->where('sponsors.userid', Auth::id())
// ->where('donations.patientid', null)
// ->select(DB::raw("SUM(sponsors.voucherValue) as sum"))
// ->get();


$helpxp = DB::table('sponsors')
->where('sponsors.status', 'dd-hx')
->orWhere('sponsors.status', 'd-hx')
->select(DB::raw("SUM(sponsors.voucherValue) as sum"))
->get();


//redeem
       
        $redeemdetails = Patient::where('userid', Auth::id())->where('status', 'full')->get();

        return view('history')->with(['sponsorCollect'=>$sponsorCollect, 'redeemdetails'=>$redeemdetails, 'helpxp'=>$helpxp]);
    }

    public function total(Request $request){
    $user = Patient::select('userid')->where('userid', Auth::id())->get();
    $pnt = Patient::where('status', 'partial')->orWhere('status', 'approved')->whereIn('userid', $user)->get();

    $patient = new Collection();
    foreach($pnt as $p){
        if($p->TotalRedeem - $p->redeemed > 0){
            $patient->push($p);
        }
    }
    return $patient;
    }

    public function sharedStories(Request $request){
       $data = Patient::whereRaw('goal != TotalRedeem')->orWhereRaw('goal - TotalRedeem > 100 and goal - TotalRedeem > 0')->get();
        return view('welcome')->with(['data'=>$data]);
    }


    public function storylist(){
     

       
    $patient = Patient::where('userid', Auth::id())->where('status', '!=', 'pending')
    ->whereRaw('goal != TotalRedeem')->get();
        $success = Patient::where('userid', Auth::id())->whereRaw('goal = TotalRedeem')->get();
        return view('mystory')->with(['patient'=>$patient, 'success'=>$success]);
    }

    public function filterDonations($from, $to){
     $donation = DB::table('donations')
            ->join('patients', 'patients.patientid', 'donations.patientid')
            ->join('sponsors', 'sponsors.sponsor_serial', 'donations.sponsor_serial')
            ->join('users', 'users.id', 'patients.userid')
            ->where('sponsors.userid', Auth::id())
            ->whereBetween(DB::raw('DATE(sponsors.updated_at)'), array($from, $to))
            ->select('sponsors.voucherValue', 'sponsors.updated_at', 'users.fname', 'users.lname', 'patients.patientid')
            ->get();

    return $donation;
                
    }   

    public function recommend($cat, $ill){
        if($cat == 'most donated'){
            $pnt = DB::table('donations');
                return $pnt;
        }

    }

    public function get(){
        $notification = Auth()->user()->unreadNotifications;
        return $notification;
    }

}
 


 // $donation = DB::table('donations')
 //            ->join('patients', 'patients.patientid', 'donations.patientid')
 //            ->join('sponsors', 'sponsors.sponsor_serial', 'donations.sponsor_serial')
 //            ->join('users', 'users.id', 'patients.userid')
 //            ->where('sponsors.userid', Auth::id())
 //            ->whereBetween(DB::raw('DATE(donations.created_at)'), array($from, $to))
 //            ->select('sponsors.voucherValue', 'donations.created_at', 'users.fname', 'users.lname', 'patients.patientid')
 //            ->get();