<?php

namespace App\Http\Controllers;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use App\Sponsor;
use App\Patient;
use App\User;
use App\Billing;
use App\Donation;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use DB;
use App\Notifications\patientNotification;
use App\Notifications\newPatient;

class SponsorController extends Controller
{
  public function buyvoucher(){
        return view('buyvoucher');
    }

    public function savevoucher(Request $request){
      $total = 0;
      if($request->qty100 != null){
        $total += $request->qty100 * 100;
      }elseif($request->qty500 != null){
        $total += $request->qty500 * 500;
      }elseif($request->qty1000 != null){
        $total += $request->qty1000 * 1000;
      }elseif($request->qty5000 != null){
        $total += $request->qty5000 * 5000;
      }
    if(($request->qty100 < 0 || $request->qty500 < 0 || $request->qty1000 < 0 || $request->qty5000 < 0) || ($request->qty100 == null && $request->qty500 == null && $request->qty1000 == null && $request->qty5000 == null)){
      return Redirect::back()->with('error', true);
    }else{
        $billing = new Billing();
        $name = Auth::user()->fname." ".Auth::user()->lname;
        $billing->userid = Auth::id();
        $billing->receipt = $request->receipt;
        $billing->accountHolder = $name;
        $billing->ifReceived= "pending";
        $billing->save();
      for($i = 0; $i < $request->qty100; $i++){
        $user = new Sponsor();
        $user->userid = Auth::id();
        $user->billing_id = $billing->billing_id;
        $user->voucherValue = 100;
        $user->status = "pending";
        $user->save();
      }
      for($i = 0; $i < $request->qty500; $i++){
        $user = new Sponsor();
        $user->userid = Auth::id();
        $user->billing_id = $billing->billing_id;
        $user->voucherValue = 500;
        $user->status = "pending";
        $user->save();
      }  
        for($i = 0; $i < $request->qty1000; $i++){
        $user = new Sponsor();
        $user->userid = Auth::id();
        $user->billing_id = $billing->billing_id;
        $user->voucherValue = 1000;
        $user->status = "pending";
        $user->save();;
        }
        for($i = 0; $i < $request->qty5000; $i++){
        $user = new Sponsor();
        $user->userid = Auth::id();
        $user->billing_id = $billing->billing_id;
        $user->voucherValue = 5000;
        $user->status = "pending";
        $user->save();
        }

      $user_p = User::find(Auth::id());
      $arr_add = array_add($user_p, 'total', $total);
      \Notification::send($user_p, new newPatient($user_p));
        return Redirect::back()->with('success', true);
      }
      

          
    }

public function savevoucherpaypal(Request $request){

  if(!is_null($request->qty1000)){
    $qty1000 = $request->qty1000 * 1000;
  }else{
    $qty1000 = 0;
  }
  if(!is_null($request->qty500)){
    $qty500 = $request->qty500 * 500;
  }else{
    $qty500 = 0;
  }if(!is_null($request->qty100)){
    $qty100 = $request->qty100 * 100;
  }else{
    $qty100 = 0;
  }
  if(!is_null($request->qty5000)){
    $qty5000 = $request->qty5000 * 5000;
  }else{
    $qty5000 = 0;
  }
  $total = $qty1000 + $qty500 + $qty5000 + $qty100;
  
  if($request->qty100 < 0 || $request->qty500 < 0 || $request->qty1000 < 0 || $request->qty5000 < 0){
      return Redirect::back()->with('error', true);
  }elseif($request->qty100 == 0 && $request->qty500 == 0 && $request->qty1000 == 0 && $request->qty5000 == 0){
      return Redirect::back()->with('nonzero', true);
  }elseif(Auth::user()->paypal_balance < $total){
      return Redirect::back()->with('notenough', true);
  }else{
      for($i = 0; $i < $request->qty100; $i++){
        $user = new Sponsor();
        $user->userid = Auth::id();
        $user->voucherValue = 100;
       
        $user->save();
      }
      for($i = 0; $i < $request->qty500; $i++){
        $user = new Sponsor();
        $user->userid = Auth::id();
        $user->voucherValue = 500;
       
        $user->save();
      }  
        for($i = 0; $i < $request->qty1000; $i++){
        $user = new Sponsor();
        $user->userid = Auth::id();
        $user->voucherValue = 1000;
       
        $user->save();;
        }
        for($i = 0; $i < $request->qty5000; $i++){
        $user = new Sponsor();
        $user->userid = Auth::id();
        $user->voucherValue = 5000;
       
        $user->save();
        }

        $user = User::findorfail(Auth::id());
        $user->paypal_balance = $user->paypal_balance - $total;
        $user->save();
    }
     return Redirect::back()->with('success', true);
}

      
  public function newSponsor($patientid){
        $patient = Patient::findorfail($patientid);
        $data = Patient::where('status', 'approved')->whereRaw('goal != TotalRedeem')->get();   
        return view('sponsorDonate')->with(['patient'=>$patient, 'data'=>$data]);
    }



  public function getDen()
    {
       $overall = 0;
 
$countvalue = Sponsor::select('voucherValue')->where('userid', Auth::id())->where('status', null)->distinct()->get();
      $value = array();
      $d = new Collection();
      foreach ($countvalue as $key) {
        $cnt = Sponsor::where('voucherValue',$key->voucherValue)->where('userid', Auth::id())->where('status', null)->get()->count();
        $total = $key->voucherValue * $cnt;
        $overall = $total+=$overall;

        if($key->voucherValue == 100){
          $value['value'] = '/images/v_100.jpg';
        }else if($key->voucherValue == 500){
          $value['value'] = '/images/v_500.jpg';
        }else if($key->voucherValue == 1000){
          $value['value'] = '/images/v_1000.jpg';
        }else if($key->voucherValue == 5000){
          $value['value'] = '/images/v_5000.jpg';
        }

        $value['count'] = $cnt;
        $value['total'] = (string)$overall;
        $d->push($value);    
      }

      return $d;
  }


    public function saveSponsor(Request $request){ 
      $patient = Patient::findorfail($request->patientid);
      $lacking = $patient['goal'] - $patient['TotalRedeem'];

      $sponsor = Sponsor::select('voucherValue', 'sponsor_serial')->where('userid', Auth::id())->where('status', null)->orderBy('voucherValue', 'desc')->get();
      $name = Auth::user()->fname." ".Auth::user()->lname;
      $radio = Input::get('anonymous') == true ? $request->anonymous:$name;
      $amount = $request->amount;
      $total = $sponsor->sum('voucherValue'); 
      //if zero ang input
      // if($amount == 0){
      //   return Redirect::back()->with('message', true);
      // }

      
      // $collect = new Collection();
      $V = new Collection();
       $amt = $amount;

if($amt <= $total && $amount != 0){
        foreach($sponsor as $c){ 
          if($amt - $c['voucherValue'] >= 0){
            $V->push($c);
            $amt -= $c['voucherValue']; 
          }
        }
     
      $avblVoucher =  $V->sum('voucherValue');
     if($lacking < $amount){
      $donateCollection = new Collection();
      $excess = new Collection();
      foreach($V as $c){ 
          if($lacking - $c['voucherValue'] >= 0){
            $donateCollection->push($c);
            $lacking -= $c['voucherValue']; 
          }else{
            $excess->push($c);
          }
        }


        $toPatient = $donateCollection->sum('voucherValue') + $lacking;
        $total = $patient->TotalRedeem + $toPatient;
        $patient->TotalRedeem = $total;
        $patient->save();

        $arr_add = array_add($patient, 'storytitle', $patient->stories[0]->storytitle);
        if($patient->goal == $patient->TotalRedeem){
          $user_p = User::find($patient->userid);
          \Notification::send($user_p, new patientNotification($patient));
        }

        foreach($donateCollection as $donate){
          $sponsor = Sponsor::findorfail($donate['sponsor_serial']);
          $sponsor->status = "donated";
          $sponsor->save();
          $donation = new Donation();
          $donation->patientid = $patient->patientid;
          $donation->sponsor_serial = $donate['sponsor_serial'];
          $donation->sponsorName = $radio;
          $donation->save();
        }

        $lack = $lacking;
        foreach($excess as $ex){
          $excessValue = $ex['voucherValue'] - $lack;
          $sponsor = Sponsor::findorfail($ex['sponsor_serial']);
          $sponsor->status = "donated";
          $sponsor->save();
          $donation = new Donation();
          $donation->patientid = $patient->patientid;
          $donation->sponsor_serial = $ex['sponsor_serial'];
          $donation->sponsorName = $radio;
          $donation->amountDonated = $lack;
          $donation->excessDonation = $excessValue;
          $donation->save();
          $lack = $lack - $lack;
        }
          return Redirect::back()->with('excess', true); 
        }
        //if okay
      else if($amt == 0 && $lacking >= $amount){

        $total = $patient->TotalRedeem + $avblVoucher;
        $patient->TotalRedeem = $total;
        $patient->save();
          foreach ($V as $donate){
          $s = Sponsor::find($donate['sponsor_serial']);
          $s->status = "donated";
          $s->save();
          $donation = new Donation();
          $donation->patientid = $patient->patientid;
          $donation->sponsor_serial = $donate['sponsor_serial'];
          $donation->sponsorName = $radio;
          $donation->save();
        }
        $arr_add = array_add($patient, 'storytitle', $patient->stories[0]->storytitle);
        if($patient->goal == $patient->TotalRedeem){
          $user_p = User::find($patient->userid);
          \Notification::send($user_p, new patientNotification($patient));
        }
          return Redirect::back()->with('success', true); 
          //if lacking iyang mga vouchers
        }else if ($amount != 0){      
        return Redirect::back()->with('info', true)->with('avblVoucher', $avblVoucher);
        }
        // else
        //   return Redirect::back()->with('alert', true);
}

// return Redirect::back()->with('notenough', true);
    }



    public function saveSponsorAny(Request $request){
      $amount = $request->amount;
      $sponsor = Sponsor::select('voucherValue', 'sponsor_serial')->where('userid', Auth::id())->where('status', null)->orderBy('voucherValue', 'desc')->get();  
      $total = $sponsor->sum('voucherValue'); 
      $name = Auth::user()->fname." ".Auth::user()->lname;
      $radio = Input::get('anonymous') == true ? $request->anonymous:$name;
      // if($amount == 0){
      //   return Redirect::back()->with('message', true);
      // }

      $collect = new Collection();
      $V = new Collection();
      // $amount = $amount;
if($amount <= $total){
        foreach($sponsor as $c){ 
          if($amount - $c['voucherValue'] >= 0){
            $V->push($c);
            $amount -= $c['voucherValue'];
          }
        }
        

        $avblVoucher =  $V->sum('voucherValue');
        if($amount == 0){
          foreach ($V as $donate){
          $s = Sponsor::find($donate['sponsor_serial']);
          $s->status = "d-hx";
          $s->save();
          // $donation = new Donation();
          // $donation->sponsor_serial = $donate['sponsor_serial'];
          // $donation->sponsorName = $radio;
          // $donation->save();
        }
          return Redirect::back()->with('success', true); 
        }
        else if ($avblVoucher != 0){    
          return Redirect::back()->with('info', true)->with('avblVoucher', $avblVoucher);
        }
        // else
        //   return Redirect::back()->with('alert', true);
}
// return Redirect::back()->with('notenough', true);
    }

   
    public function newSponsorAny(){
      

     $data = Patient::where('status', 'approved')->whereRaw('goal != TotalRedeem')->get();

         return view('donateAny')->with(['data'=>$data]);
    }

     public function getValue()
    {
      return view('viewvouchers');
    }


}
