<?php
namespace App\Http\Controllers;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Paginator;
use App\Patient;
use App\User;
use App\Picture;
use App\Donation;
use App\Sponsor;
use App\Redeem;
use App\Stories;
use Carbon\Carbon;
use Illuminate\Support\Facades\Redirect;
use App\Notifications\NotifyUsers;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use App\Notifications\patientNotification;
use App\Notifications\patientRedeemed;
use App\Notifications\newPatient;

class PatientController extends Controller
{
    public function redeem(Request $request){

        $redeem = Patient::findorfail($request->id);
        if($redeem['goal'] != $redeem['TotalRedeem']) {
            $redeem->status = "partial";
            $redeem->expirydateV = $request->expirydate;
            $redeem->newgoal = $redeem['goal'] - $redeem['TotalRedeem'];
            $redeem->redeemed = $redeem['goal'] - $redeem['newgoal'];
            $redeem->save();
        }
        else {
            $redeem->status = "full";
            $redeem->expirydateV = $request->expirydate;
            $redeem->newgoal = $redeem['goal'] - $redeem['TotalRedeem'];
            $redeem->redeemed = $redeem['goal'] - $redeem['newgoal'];
            $redeem->save();
        }

        $arr_add = array_add($redeem, 'storytitle', $redeem->stories[0]->storytitle);
          $user_p = User::where('role', 'admin')->get();
          \Notification::send($user_p, new patientRedeemed($redeem));

     $sponsor = Sponsor::where('userid', Auth::id())->get();
$donation = Donation::where('patientid', '!=', null)->get();
$sponsorCollect = new Collection();
foreach($sponsor as $spr){
    foreach($donation as $dnr){
        if($dnr['sponsor_serial'] == $spr['sponsor_serial']){
            $sponsorCollect->push($spr);
            }
        }
}

$helpxp = DB::table('donations')
->join('sponsors', 'sponsors.sponsor_serial', 'donations.sponsor_serial')
->where('sponsors.userid', Auth::id())
->where('donations.patientid', null)
->select(DB::raw("SUM(sponsors.voucherValue) as sum"))
->get();



\Session::put('success', true);

//redeem
       
        $redeemdetails = Patient::where('userid', Auth::id())->where('status', 'redeem')->get();

        return view('history')->with(['sponsorCollect'=>$sponsorCollect, 'redeemdetails'=>$redeemdetails, 'helpxp'=>$helpxp]);
 
    }


    public function voucher($patientid){
        $details = Patient::findorfail($patientid);
        $total = $details->TotalRedeem - $details->redeemed;
        // $details = Patient::where('userid', $user)->where('status', 'approved')->get();
        // $expirydate = date('Y-m-d', strtotime('+1 years'));
        return view('confirm')->with(['details'=>$details, 'total'=>$total]);
    }




    public function displayPatient(Request $request){
        if(Auth::user()->role == "admin") {
            $pnt = Patient::get();

$data = Patient::whereRaw('goal != TotalRedeem')->orWhereRaw('goal - TotalRedeem > 100 and goal - TotalRedeem > 0')->get();

        $success = Patient::whereRaw('goal = TotalRedeem')->orWhereRaw('goal - TotalRedeem < 100 and goal - TotalRedeem > 0')->get();
            return view('displayUsers')->with(['data'=>$data, 'success'=>$success]);
        }

        $data = Patient::whereRaw('goal != TotalRedeem')->orWhereRaw('goal - TotalRedeem > 100 and goal - TotalRedeem > 0')->get();

        $success = Patient::whereRaw('goal = TotalRedeem')->orWhereRaw('goal - TotalRedeem < 100 and goal - TotalRedeem > 0')->get();
      
            return view('homepage')->with(['data'=>$data, 'success'=>$success]);
            

        
    }

   
    public function patient($patientid){
	$patient = Patient::findorfail($patientid); 
    $patient->views = $patient->views + 1;
    $patient->save();
    $story = Stories::where('patientid', $patient['patientid'])->first();
   
    $pic = Picture::where('storyid', $story['storyid'])->paginate(1);
	return view('singlelist')->with(['patient'=>$patient, 'story'=>$story,'pic'=>$pic]);
	}


    public function updatepage($patientid){
    //$patient = Patient::findorfail($patientid);
    $ups = stories::where('patientid', $patientid)->where('role', null)->paginate(1);
    return view('updatepage')->with(['ups'=>$ups]);
    }



    public function newPatient(){
        
    	return view('patientsdetail');
    }
    public function savePatient(Request $request){
        // return $request;
        //$picture = Picture::all();
        $condition = Input::get('condition') == true ? $request->condition:"error";
        $id = Auth::id();
        if($request->hasFile('profile')){
            $files = $request->file('profile');
            $origextension = $files->getClientOriginalExtension();
           $origname = $files->getClientOriginalName();
           $filename = pathinfo($origname, PATHINFO_FILENAME);
           $storefile = $filename.'-'.time().'.'.$origextension;
            $files->storeAs('public/picture', $storefile);
            
            
        $patient = new Patient();
        $patient->userid = $id;
        $patient->goal = $request->goal;
        $patient->filename = $storefile;
        $patient->status = "pending";
        $patient->patientname = ucfirst($request->bname);
        $patient->illness = ucfirst($request->illness);
        $patient->condition = $condition;
        $patient->save();

        }else{
            return Redirect::back()->with('error', true);
        }


        $story = new Stories();
        $story->patientid = $patient->patientid;
        $story->storytitle = $request->title;
        $story->story = $request->story;
        $story->role = "main";
        $story->save();
    	$insertid = $patient->patientid;
        

        if($request->hasFile('medicalAbstract')){
            $files = $request->file('medicalAbstract');
            // foreach($files as $file){

           $origextension = $files->getClientOriginalExtension();
           $origname = "medicalAbstract";
           $filename = pathinfo($origname, PATHINFO_FILENAME);
           $storefile = $filename.'-'.time().'.'.$origextension;

            $files->storeAs('public/picture', $storefile);


            $picture = new Picture();
            $count = $picture::count();
            $picture->picid = "pic".$count;
            $picture->filename = $storefile;
            $picture->storyid = $story->storyid;
            $picture->save();
            // }
        }else{
            return Redirect::back()->with('error', true);
        }


        if($request->hasFile('medicalCertificate')){
            $files = $request->file('medicalCertificate');
            // foreach($files as $file){

           $origextension = $files->getClientOriginalExtension();
           $origname = "medicalCertificate";
           $filename = pathinfo($origname, PATHINFO_FILENAME);
           $storefile = $filename.'-'.time().'.'.$origextension;

            $files->storeAs('public/picture', $storefile);


            $picture = new Picture();
            $count = $picture::count();
            $picture->picid = "pic".$count;
            $picture->filename = $storefile;
            $picture->storyid = $story->storyid;
            $picture->save();
            // }
        }else{
            return Redirect::back()->with('error', true);
        }

        if($request->hasFile('validID')){
            $files = $request->file('validID');
            // foreach($files as $file){

           $origextension = $files->getClientOriginalExtension();
           $origname = "validID";
           $filename = pathinfo($origname, PATHINFO_FILENAME);
           $storefile = $filename.'-'.time().'.'.$origextension;

            $files->storeAs('public/picture', $storefile);


            $picture = new Picture();
            $count = $picture::count();
            $picture->picid = "pic".$count;
            $picture->filename = $storefile;
            $picture->storyid = $story->storyid;
            $picture->save();
            // }
        }else{
            return Redirect::back()->with('error', true);
        }

        if($request->hasFile('hospitalBill')){
            $files = $request->file('hospitalBill');
            // foreach($files as $file){

           $origextension = $files->getClientOriginalExtension();
           $origname = "hospitalBill";
           $filename = pathinfo($origname, PATHINFO_FILENAME);
           $storefile = $filename.'-'.time().'.'.$origextension;

            $files->storeAs('public/picture', $storefile);


            $picture = new Picture();
            $count = $picture::count();
            $picture->picid = "pic".$count;
            $picture->filename = $storefile;
            $picture->storyid = $story->storyid;
            $picture->save();
            // }
        }else{
            return Redirect::back()->with('error', true);
        }

        if($request->hasFile('breakdownExpenses')){
            $files = $request->file('breakdownExpenses');
            // foreach($files as $file){

           $origextension = $files->getClientOriginalExtension();
           $origname = "breakdownExpenses";
           $filename = pathinfo($origname, PATHINFO_FILENAME);
           $storefile = $filename.'-'.time().'.'.$origextension;

            $files->storeAs('public/picture', $storefile);


            $picture = new Picture();
            $count = $picture::count();
            $picture->picid = "pic".$count;
            $picture->filename = $storefile;
            $picture->storyid = $story->storyid;
            $picture->save();
            // }
        }else{
            return Redirect::back()->with('error', true);
        }

        $arr_add = array_add($patient, 'storytitle', $patient->stories[0]->storytitle);
        $user_p = User::where('role', 'admin')->get();
        \Notification::send($user_p, new newPatient($patient));

    	return redirect(url('/list/'.$insertid.'/view/'));
    }




    public function updateStory($patientid){
        return view('update')->with(['patient'=>$patientid]);
    }

    public function saveupdateStory(Request $request){
        $story = new Stories();
        $story->patientid = $request->patientid;
        $story->storytitle = $request->updatetitle;
        $story->story = $request->story;
        $story->role = null;
        $story->save();
        $insertid = $request->patientid;
        

        if($request->hasFile('file')){
            $files = $request->file('file');
            foreach($files as $file){

           $origextension = $file->getClientOriginalExtension();
           $origname = $file->getClientOriginalName();
           $filename = pathinfo($origname, PATHINFO_FILENAME);
           $storefile = $filename.'-'.time().'.'.$origextension;

            $file->storeAs('public/picture', $storefile);


            $picture = new Picture();
            $count = $picture::count();
            $picture->picid = "pic".$count;
            $picture->filename = $storefile;
            $picture->storyid = $story->storyid;

            $picture->save();
            }
        }

        return redirect(url('/update/'.$insertid.'/view/'));
    }



    public function donors($patientid){
        $patient = Patient::findorfail($patientid);

        //total of all donations
        foreach($patient as $pnt){
             $donors = Donation::where('patientid', $patientid)->get(); 
        }
        $total = 0;
        foreach($donors as $amount){
            $count = $amount->sponsor->voucherValue;
            $total += $count;
        }

        foreach($patient as $pnt){
             $nonanonymous = Donation::where('patientid', $patientid)->where('sponsorName','!=', 'anonymous')->get(); 
        }

        //for all total sa anonymous donations
        foreach($patient as $pnt){
             $anonymous = Donation::where('patientid', $patientid)->where('sponsorName', 'anonymous')->get(); 
        }
        $totalanonymous = 0;
        foreach($anonymous as $amount){
            $count = $amount->sponsor->voucherValue;
            $totalanonymous += $count;
        }
       
        return view('donors')->with(['total'=>$total, 'totalanonymous'=>$totalanonymous, 'nonanonymous'=>$nonanonymous]);
    }

}
