<?php

namespace App\Http\Controllers;


use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Collection;
use App\Http\Requests;
use App\Http\Controllers\AdminHomeController as admin;
use Auth;
use App\User;
use App\Stories;
use Session;
use App\Picture;
use App\Patient;
use App\Sponsor;
use App\Donation;
use App\Billing;
use App\Criteria;
use Carbon\Carbon;
use DB;
use App\Events\Reco;
use AppHelper;
use App\Jobs\Recommendation;
use Mail;
use App\Notifications\patientConfirm;
use App\Notifications\patientNotification;
use App\Notifications\voucherConfirm;
class AdminHomeController extends Controller
{ 

    public function adminLogin(Request $req) {
        // AppHelper::setreco();
        //return $req;
     

        if (Auth::attempt(['username' => $req->username, 'password' => $req->password, 'role' => 'admin']))
        {

            return view('displayUsers');
        }
        else
            return view('auth.admin-login');
    }

    public function viewUsers()
    {
        // AppHelper::setreco();
        if(Auth::user()->role == "admin"){
         $data = Patient::where('status', 'approved')->whereRaw('goal != TotalRedeem')->WhereRaw('goal - TotalRedeem > 100 and goal - TotalRedeem > 0')->get();
        $success = Patient::whereRaw('goal = TotalRedeem')->orWhereRaw('goal - TotalRedeem < 100 and goal - TotalRedeem > 0')->get();
            return view('displayUsers')->with(['data'=>$data, 'success'=>$success]);

        }
        else
            return "ERROR!";

    }

    // public function deleteUsers(Request $request)
    // {
    //    $user = User::findOrFail($request->id);
    //    $user->delete();
    //    return redirect(url('/displayusers'));
    // }

    public function viewPatients()
    {
        // AppHelper::setreco();
        if(Auth::user()->role == "admin"){
            $users = Patient::select('userid')->distinct('userid')->get();
            return view('displaypatients', compact('users'));
    }
        else
            return "ERROR!";
    }

    public function viewSponsors()
    {
        // AppHelper::setreco();
        if(Auth::user()->role == "admin"){
            $users = Sponsor::select('userid')->distinct('userid')->get();
            return view('displaysponsors', compact('users'));
            }
        else
            return "ERROR!";
    }
    

    public function patientSponsor($userid) {
        // AppHelper::setreco();
       //   $id = Donation::where('patientid', '=', $userid)->get();
       // return view('displayPatientSponsor')->with([ 'patientid' => $id]);

        if(Auth::user()->role == "admin"){
        $patient = Patient::where('userid', $userid)->get();
        $voucher = Donation::get();
        $patientCollect = new Collection();
        foreach($patient as $pts) {
            foreach ($voucher as $vcr) {
                if($pts['patientid'] == $vcr['patientid']) {
                    $patientCollect->push($vcr);
                 
                }
            }
            
        }
      
       return view('displayPatientSponsor')->with([ 'patientCollect' => $patientCollect]);
    }
    else
        return "ERROR!";

    }

    public function sponsorSponsored($userid) {
        // AppHelper::setreco();
        if(Auth::user()->role == "admin"){
        $sponsor = Sponsor::where('userid', $userid)->get();
        $voucher = Donation::get();
        $sponsorCollect = new Collection();
        foreach($sponsor as $spr){
            foreach($voucher as $dnr){
                if($dnr['sponsor_serial'] == $spr['sponsor_serial']){
                $sponsorCollect->push($spr);
                }
            }
        }

        return view('displaySponsorSponsored')->with(['sponsorCollect'=>$sponsorCollect]);
    }

    else return "ERROR!";
    }

    public function patienthistory($userid) {
        // AppHelper::setreco();
        if(Auth::user()->role == "admin"){
        $user = Auth::id();
        $patients = Patient::where('userid', "=" ,$userid)->get();
        return view('patienthistory')->with(['user'=>$patients]);
    }
        else 
            return "ERROR";
    }

    public function sponsorhistory($userid) {
        // AppHelper::setreco();
        if(Auth::user()->role == "admin"){
        $user = Auth::id();
        $sponsors = Sponsor::where('userid', "=" ,$userid)->get();
        return view('sponsorhistory')->with(['user'=>$sponsors]);
         }
        else 
            return "ERROR";
    }

    public function approveStories() {
        // AppHelper::setreco();
        if(Auth::user()->role == "admin"){
            $patient = Patient::where('status', 'pending')->get();
            $approve = new Collection();
            foreach($patient as $p){
                $story = $p->stories[0];
                $approve->push($story);
            }
            return view('approveStories')->with(['approve'=>$approve]);
        }
        else 
            return "ERROR";

    }

    public function approved(Request $request) {
        // AppHelper::setreco();
        if(Auth::user()->role == "admin"){
            $pnt = Patient::findOrFail($request->patientid);
            $pnt->status = "approved";
            $pnt->save();

            // $arr_add = array_add($pnt, 'storytitle', $pnt->stories[0]->storytitle);
            User::find($pnt->userid)->notify(new patientConfirm($pnt));
            // \Notification::send($user_p, new patientConfirm($pnt));

            $patient = Patient::where('status', 'pending')->get();
            $approve = new Collection();
            foreach($patient as $p){
                $story = $p->stories[0];
                $approve->push($story);
            }
            return view('approveStories')->with(['approve'=>$approve]);
        }

        else 
            return "ERROR";

    }

    

    public function filterSponsorDate(Request $request){ 
        // AppHelper::setreco();
        if(Auth::user()->role == "admin"){
        $from = $request->from;
        $to = $request->to;
        $users = sponsor::select('userid')->distinct('userid')->whereBetween(DB::raw('DATE(created_at)'), array($from, $to))->get();
         return view('displaySponsors')->with(['users'=>$users]);
         }
        else 
            return "ERROR";
    }

    public function requestRedeem()
    {
        // AppHelper::setreco();
        if(Auth::user()->role == "admin") {
         $patient = Patient::where('status', 'partial')->orWhere('status', 'full')->get();
            $redeem = new Collection();
            foreach($patient as $p){
                $pnt = $p->stories[0];
                $redeem->push($pnt);
            }

        $user = Auth::id();    
$released = Patient::where('userid', $user)->where('status', 'partial')->orWhere('status', 'full')->get();

$expirydate = date('Y-m-d', strtotime('+1 years'));
            return view('reqRedeems')->with(['redeem'=>$redeem, 'released'=>$released, 'expirydate'=>$expirydate]);
        }
        else
            "ERROR";
    }

    public function reqConfirm(Request $request){
        // AppHelper::setreco();

        $details = Patient::findorfail($request->patientid);
        $total = $details->redeemed;
        // $details = Patient::where('userid', $user)->where('status', 'approved')->get();
        $expirydate = date('d M Y', strtotime('+1 years'));
        return view('confirmreq')->with(['details'=>$details, 'total'=>$total, 'expirydate'=>$expirydate]);
    }


    public function requestApproveRedeem(Request $request)
    {
        // AppHelper::setreco();
        if(Auth::user()->role == "admin"){
        $patient = Patient::where('status', 'partial')->orWhere('status', 'full')->get();

            $pnt = Patient::findOrFail($request->patientid);
            $pnt->redeemStatus = "Release";
            $pnt->save();
            
            $redeem = new Collection();
            foreach($patient as $p){
                $pnt = $p->stories[0];
                $redeem->push($pnt);
            }

            return view('reqApproveRedeems')->with(['redeem'=>$redeem]);
            }

        
       
        else
            return "ERROR!";

    }

    public function filterPatientDate(Request $request){ 
        // AppHelper::setreco();
        if(Auth::user()->role == "admin"){
        $from = $request->from;
        $to = $request->to;
        $users = patient::select('userid')->distinct('userid')->whereBetween(DB::raw('DATE(created_at)'), array($from, $to))->get();
         return view('displayPatients')->with(['users'=>$users]);
         }
        else 
            return "ERROR";
    }

        // angel
    public function checkPayment() {
        // AppHelper::setreco();
        if(Auth::user()->role == "admin"){
            $bill = Billing::where('ifReceived','=','pending')->get();
            $check = new Collection();
            foreach($bill as $bills){
                $candonate = $bills->sponsors[0];
                $check->push($candonate);
            }
            return view('check')->with(['bill'=>$bill]);
        }
        else 
            return "ERROR";

    }

    public function checked(Request $request) {
        // AppHelper::setreco();
        if(Auth::user()->role == "admin"){
            $bill = Billing::find($request->billing_id);
            $bill->ifReceived = "received";
            $bill->save();

            $sponsor = Sponsor::where('billing_id', $request->billing_id)->get();
            foreach ($sponsor as $sponsors) {
              $sponsors->status = null;
              $sponsors->save();
            }       

            $user_p = User::find($request->userid);
            $arr_add = array_add($user_p, 'time', Carbon::now()->format('F d, Y h:i a'));
            \Notification::send($user_p, new voucherConfirm($user_p));    

            $sponsor = Sponsor::where('status', null)->get();
            $approve = new Collection();
            foreach($sponsor as $sponsors){
                $candonate = $sponsors->sponsors[0];
                $approve->push($candonate);
            }

            return view('checkedPayment')->with(['sponsor'=>$sponsor]);
        }

        else 
            return "ERROR";
    }

// angel new
    public function searchPatient(Request $request){
        // AppHelper::setreco();
        $search = $request->search;
        $patient = Patient::where('illness','LIKE', '%' .$search. '%')->get();
        if(count($patient)>0){
          return view('search')->with(['patient' => $patient]);  
      }else
        return Redirect::back()->with('message', "No Results Found! Try another...");      
    }

    public function newStories(){
        // AppHelper::setreco();
        $new = Patient::where('status','=','approved')->whereDate('created_at',Carbon::today())->get();
        if(count($new)>0){
            return view('newStories')->with(['new' => $new]);
        } else
        return Redirect::back()->with('noNew', "There are no new stories as of the moment! Try another...");    
    }
 //end

public function donate(){
    //  $carbon = now();
    // $date = $carbon->format('H:i:s');
    //     event(new Reco($date));

   
// $yesterday = date('Y-m-d', time() - 60 * 60 * 24);
// //list of recommended patients
// $sponsored = Sponsor::select('sponsor_serial')->where('status', 'dd-hx')->get();
// $sponsored_pnt = Donation::whereIn('sponsor_serial', $sponsored)
// ->whereDate('created_at','=', $yesterday)
// ->get();
$sponsee = new Collection();
  $today = date('Y-m-d', strtotime('-1 day'));
     $yesterday = date('Y-m-d', time() - 60 * 60 * 24);
    $donate = Donation::select('patientid')->distinct('patientid')->
    whereDate('created_at', $today)->get();
    foreach($donate as $sponsor=>$s){
        $total = 0;
        $ss = new Collection();
            $don_tbl = Donation::where('patientid', $s->patientid)->whereDate('created_at', $today)->get();
            $sponsee->push($don_tbl);
            foreach($don_tbl as $don){
                $spon_tbl = Sponsor::where('sponsor_serial', $don->sponsor_serial)->get();
                $total += $spon_tbl[0]->voucherValue; 
                $user = Patient::findOrFail($don->patientid);
                $s['patientid'] = $user->patientid;
                $s['patientname'] = $user->patientname;
            }
        $s['total'] = $total;
        $s['created_at'] = $don_tbl[$don_tbl->count()-1]->updated_at;
   }
   $donate = $donate->sortByDesc('total');
  foreach($donate as $donor){
    foreach($sponsee as $spon){
        if($donor['patientid'] == $spon[0]->patientid){
            $donor['voucher'] = $spon;
        }
    }
  }

$criteria = Criteria::all();


// AppHelper::setreco();
   
// Recommendation::dispatch();
// $schedule =job(new Recommendation)->daily()->between('4:00', '18:00');
// $recoJob = (new Recommendation())->delay(Carbon::now()->addSeconds(3));
// dispatch($recoJob);

return view('donate')->with(['donate'=>$donate, 'criteria'=>$criteria]);       
}

public function getrecoresults(){
    // return $object;
$criteria_p = Criteria::all();

$donation = Donation::select('sponsor_serial')->where('sponsor_serial', '!=', null)->get();
$tohelpxp = Sponsor::where('status', 'd-hx')->whereNotIn('sponsor_serial', $donation)->orderBy('voucherValue', 'desc')->get();
$count_helpxp = $tohelpxp->count();
$sum = $tohelpxp->sum('voucherValue');
//if tanan patient kay nahatagan na
if(Patient::where('flag', '1')->count() == Patient::count()){
    $p_all = Patient::all();
    foreach($p_all as $all){
        $save = Patient::findorfail($all->patientid);
        $save->flag = 0;
        $save->save();
    }
}elseif(Patient::where('flag', 0) != null){
    $p_all = Patient::where('flag', 0)->get();
    $count = 0;
    foreach($p_all as $all){
        $lack = $all->goal - $all->TotalRedeem;
        if($lack == 0){
            $count++;
        }
    }

    if($p_all->count() == $count){
        foreach($p_all as $all){
            $save = Patient::findorfail($all->patientid);
            $save->flag = 1;
            $save->save();
        }
    }
}

//*most sponsor donated
$mostDonated = Sponsor::select('userid')->distinct()->where('status', 'donated')->orWhere('status', 'd-hx')->get();
$most_donated = new Collection();
foreach($mostDonated as $md){
    $spnr = Sponsor::where('userid', $md->userid)->where('status', 'd-hx')->orWhere('status', 'donated')->orWhere('status', 'dd-hx')->count();
    $most['count'] = $spnr;
    $most['userid'] = $md->userid;
    $most_donated->push($most);
}
$sort_most_donated = $most_donated->sortByDesc('count');
//*find patients under this userid's
$patient_withdonation = new Collection();
foreach($sort_most_donated as $most_d){
    $pnts = Patient::where('userid', $most_d['userid'])->where('flag', '0')->get();
    foreach($pnts as $p){
        $p_id = $p->userName->sponsor->count();
        $pa['count'] = $p_id;
        $pa['patientid'] = $p->patientid;
        $pa['patientname'] = $p->patientname;
        if($p->goal - $p->TotalRedeem != 0){
            $patient_withdonation->push($pa); 
        }
    }
}

$sorted_pwd = $patient_withdonation->sortByDesc('count');
$patients_list = new Collection();
$total_wd = $sorted_pwd->count();
$to_wd = $total_wd;
$total_wd_rank = 1;
foreach($sorted_pwd as $p=>$pwd){
    if($patients_list->where('patientid', $pwd['patientid'])->count() == 0){
        $find = $sorted_pwd->where('count', $pwd['count']);
        foreach($find as $f=>$fi){
            $formula = ($criteria_p[0]->percentage / $to_wd) * $total_wd;
            $wd['rank'] = $total_wd_rank;
            $wd['patientid'] = $fi['patientid'];
            $wd['patientname'] = $fi['patientname'];
            $wd['count'] = $fi['count'];
            $wd['points'] = number_format($formula, 2, '.', ',');
            $patients_list->push($wd);
        }
        $total_wd--; 
        $total_wd_rank++;
    }
}
$patients = $patients_list->take(10);

$pnt_list = new Collection();
$patient_phn = Patient::where('flag', 0)->get();
foreach($patient_phn as $pnts){
    $pat['patientid'] = $pnts['patientid'];
    $pat['patientname'] = $pnts['patientname'];
    $lacking = $pnts['goal'] - $pnts['TotalRedeem'];
    $pat['lacking'] = $lacking;
    $pat['created_at'] = $pnts['created_at'];
    $pat['condition'] = $pnts['condition'];
    $pat['goal'] = $pnts['goal'];
    $pat['TotalRedeem'] = $pnts['TotalRedeem'];
    if($lacking != 0){
         $pnt_list->push($pat);
    }
}

$sorted_pld = $pnt_list->sortBy('created_at');
$longestDuration_count = new Collection();
$total_pld = $sorted_pld->count();
$to_pld = $total_pld;
$total_pld_rank = 1;
foreach($sorted_pld as $pld){
    if($longestDuration_count->where('patientid', $pld['patientid'])->count() == 0){
    $creat = new Collection();
    $no_cre = $pld['created_at']->toDateString();
    foreach($sorted_pld as $sp){
        $cr = $sp['created_at']->toDateString();
        if($no_cre == $cr){
            $creat->push($sp);
        }
    }
        foreach($creat as $fi){
            $formula = ($criteria_p[1]->percentage / $to_pld) * $total_pld;
            $ld['rank'] = $total_pld_rank;
            $ld['created_at'] = $fi['created_at'];
            $ld['patientid'] = $fi['patientid'];
            $ld['patientname'] = $fi['patientname'];
            $ld['points'] = number_format($formula, 2, '.', ',');
            $longestDuration_count->push($ld);
        }
        $total_pld--;
        $total_pld_rank++;
    }
}
$longestDuration = $longestDuration_count->take(10);

$sorted_phn = $pnt_list->sortByDesc('lacking');
$highest_need_count = new Collection();
$total_phn = $sorted_phn->count();
$to_phn = $total_phn;
$total_phn_rank = 1;
foreach($sorted_phn as $p=>$phn){
    if($highest_need_count->where('patientid', $phn['patientid'])->count() == 0){
        $find = $sorted_phn->where('lacking', $phn['lacking']);
        foreach($find as $fi){
            $formula = ($criteria_p[2]->percentage / $to_phn) * $total_phn;
            // $percent = round((float)$formula * 100);
            $hn['rank'] = $total_phn_rank;
            $hn['patientid'] = $fi['patientid'];
            $hn['patientname'] = $fi['patientname'];
            $hn['points'] = number_format($formula, 2, '.', ',');
            $hn['lacking'] = $fi['lacking'];
            $highest_need_count->push($hn);
        }
        $total_phn--;
        $total_phn_rank++;
    }
}
$highest_need = $highest_need_count->take(10);

$sorted_c = $pnt_list->sortByDesc('condition');
$condition_count = new Collection();
$total_c = $sorted_c->count();
$to_c = $total_c;
$total_c_rank = 1;
foreach($sorted_c as $c){
    if($condition_count->where('condition', $c['condition'])->count() == 0){
        $find = $sorted_c->where('condition', $c['condition']);
        foreach($find as $fi){
            $formula = ($criteria_p[3]->percentage / $to_c) * $total_c;
            $con['rank'] = $total_c_rank;
            $con['patientid'] = $fi['patientid'];
            $con['condition'] = $fi['condition'];
            $con['patientname'] = $fi['patientname'];
            $con['points'] = number_format($formula, 2, '.', ',');
            $condition_count->push($con);
        }
        $total_c--;   
        $total_c_rank++;
    }
}
$condition = $condition_count->take(10);

$all_patients = new Collection();
foreach($longestDuration as $p_longest){
    $all_patients->push($p_longest);
}
foreach($highest_need as $p_highest){
    $all_patients->push($p_highest);
}
foreach($condition as $p_condition){
    $all_patients->push($p_condition);
}
foreach($patients as $patient){
    $all_patients->push($patient);
}

$final = new Collection();
$final_list = new Collection();
foreach($all_patients as $d){
    
    if($final_list->where('patientid', $d['patientid'])->count() == 0){
    $total_r = Patient::select('TotalRedeem', 'userid')->where('patientid', $d['patientid'])->get();
    
        $find = $all_patients->where('patientid', $d['patientid']);
        foreach($find as $f){
            $final->push($f);
        }
        $f_pnt = null;
        $f_pnt['patientid'] = $d['patientid'];
        $f_pnt['patientname'] = $d['patientname'];
        $sum = $find->sum('points');
        $f_pnt['sum'] = number_format($sum, 2, '.', ',');
        $f_pnt['flag'] = 0;
        $f_pnt['TotalRedeem'] = $total_r[0]->TotalRedeem;
        $f_pnt['userid'] = $total_r[0]->userid;


foreach($final as $fin){
   
   if(array_key_exists('lacking', $fin)){
        $f_pnt['lacking'] = $fin['points'];
        $f_pnt['lacking_num'] = $fin['lacking'];
   }if(array_key_exists('condition', $fin)){
        $f_pnt['condition'] = $fin['points'];
   }if(array_key_exists('created_at', $fin)){
        $f_pnt['created_at'] = $fin['created_at'];
        $f_pnt['c_points'] = $fin['points'];
   }if(array_key_exists('count', $fin)){   
        $f_pnt['count'] = $fin['points'];
   }
}
        $final_list->push($f_pnt); 
    }
}

// $object = (object) $finalList;
// $this->getrecoresults($object);

$sorted_all = $final_list->sortByDesc('sum');
$total_rank = 1;
$same = new Collection();
foreach($sorted_all as $s=>$s_all){
    if($same->where('patientid', $s_all['patientid'])->count() == 0){
    $same_sum = $sorted_all->where('sum', $s_all['sum'])->sortBydesc('lacking');
    foreach($same_sum as $sa=>$ss){
        $ss['rank'] = $total_rank;
        $sorted_all[$sa] = $ss;
        $total_rank++;
        $same->push($ss);
    }
    }
}

$final = $sorted_all->take(10)->toArray();

return view('recoresults')->with(['final'=>$final,'longestDuration'=>$longestDuration,'highest_need'=>$highest_need,'condition'=>$condition,'patients'=>$patients]);
}


public function percentChange($percent1,$percent2,$percent3,$percent4)     
{
    // AppHelper::setreco();
    $per1 = Criteria::findOrFail('1');
    $per2 = Criteria::findOrFail('2');
    $per3 = Criteria::findOrFail('3');
    $per4 = Criteria::findOrFail('4');

    $per1->percentage = $percent1;
    $per2->percentage = $percent2;
    $per3->percentage = $percent3;
    $per4->percentage = $percent4;

    $per1->save();
    $per2->save();
    $per3->save();
    $per4->save();
   return $percent1.$percent2.$percent3.$percent4;
}


public function helpVoucher(){
    // AppHelper::setreco();
    $overall = 0;
    $donation = Donation::select('sponsor_serial')->where('sponsor_serial', '!=', null)->get();
    $tohelpxp = Sponsor::select('voucherValue')->distinct()->where('status', 'd-hx')->whereNotIn('sponsor_serial', $donation)->orderBy('voucherValue', 'desc')->get();
    
    $d = new Collection();
     foreach ($tohelpxp as $key) {
        $cnt = Sponsor::where('voucherValue', $key->voucherValue)->where('status', 'd-hx')->whereNotIn('sponsor_serial', $donation)->get()->count();
        $total = $key->voucherValue * $cnt;
        $overall = $total+=$overall;

        if($key->voucherValue == 100){
          $value['value'] = '/images/v_100.jpg';
        }else if($key->voucherValue == 500){
          $value['value'] = '/images/v_500.jpg';
        }else if($key->voucherValue == 1000){
          $value['value'] = '/images/v_1000.jpg';
        }else if($key->voucherValue == 5000){
          $value['value'] = '/images/v_5000.jpg';
        }

        $value['count'] = $cnt;
        $value['total'] = (string)$overall;
        $d->push($value);    
      }

return $d;
}

public static function recommending(){
    // AppHelper::setreco();

$criteria_p = Criteria::all();

$donation = Donation::select('sponsor_serial')->where('sponsor_serial', '!=', null)->get();
$tohelpxp = Sponsor::where('status', 'd-hx')->whereNotIn('sponsor_serial', $donation)->orderBy('voucherValue', 'desc')->get();
$count_helpxp = $tohelpxp->count();
$sum = $tohelpxp->sum('voucherValue');
//if tanan patient kay nahatagan na
if(Patient::where('flag', '1')->count() == Patient::count()){
    $p_all = Patient::all();
    foreach($p_all as $all){
        $save = Patient::findorfail($all->patientid);
        $save->flag = 0;
        $save->save();
    }
}elseif(Patient::where('flag', 0) != null){
    $p_all = Patient::where('flag', 0)->get();
    $count = 0;
    foreach($p_all as $all){
        $lack = $all->goal - $all->TotalRedeem;
        if($lack == 0){
            $count++;
        }
    }

    if($p_all->count() == $count){
        foreach($p_all as $all){
            $save = Patient::findorfail($all->patientid);
            $save->flag = 1;
            $save->save();
        }
    }
}

//*most sponsor donated
$mostDonated = Sponsor::select('userid')->distinct()->where('status', 'donated')->orWhere('status', 'd-hx')->get();
$most_donated = new Collection();
foreach($mostDonated as $md){
    $spnr = Sponsor::where('userid', $md->userid)->where('status', 'd-hx')->orWhere('status', 'donated')->orWhere('status', 'dd-hx')->count();
    $most['count'] = $spnr;
    $most['userid'] = $md->userid;
    $most_donated->push($most);
}
$sort_most_donated = $most_donated->sortByDesc('count');
//*find patients under this userid's
$patient_withdonation = new Collection();
foreach($sort_most_donated as $most_d){
    $pnts = Patient::where('userid', $most_d['userid'])->where('flag', '0')->get();
    foreach($pnts as $p){
        $p_id = $p->userName->sponsor->count();
        $pa['count'] = $p_id;
        $pa['patientid'] = $p->patientid;
        $pa['patientname'] = $p->patientname;
        if($p->goal - $p->TotalRedeem != 0){
            $patient_withdonation->push($pa); 
        }
    }
}

$sorted_pwd = $patient_withdonation->sortByDesc('count');
$patients_list = new Collection();
$total_wd = $sorted_pwd->count();
$to_wd = $total_wd;
$total_wd_rank = 1;
foreach($sorted_pwd as $p=>$pwd){
    if($patients_list->where('patientid', $pwd['patientid'])->count() == 0){
        $find = $sorted_pwd->where('count', $pwd['count']);
        foreach($find as $f=>$fi){
            $formula = ($criteria_p[0]->percentage / $to_wd) * $total_wd;
            $wd['rank'] = $total_wd_rank;
            $wd['patientid'] = $fi['patientid'];
            $wd['patientname'] = $fi['patientname'];
            $wd['count'] = $fi['count'];
            $wd['points'] = number_format($formula, 2, '.', ',');
            $patients_list->push($wd);
        }
        $total_wd--; 
        $total_wd_rank++;
    }
}
$patients = $patients_list->take(10);

$pnt_list = new Collection();
$patient_phn = Patient::where('flag', 0)->get();
foreach($patient_phn as $pnts){
    $pat['patientid'] = $pnts['patientid'];
    $pat['patientname'] = $pnts['patientname'];
    $lacking = $pnts['goal'] - $pnts['TotalRedeem'];
    $pat['lacking'] = $lacking;
    $pat['created_at'] = $pnts['created_at'];
    $pat['condition'] = $pnts['condition'];
    $pat['goal'] = $pnts['goal'];
    $pat['TotalRedeem'] = $pnts['TotalRedeem'];
    if($lacking != 0){
         $pnt_list->push($pat);
    }
}

$sorted_pld = $pnt_list->sortBy('created_at');
$longestDuration_count = new Collection();
$total_pld = $sorted_pld->count();
$to_pld = $total_pld;
$total_pld_rank = 1;
foreach($sorted_pld as $pld){
    if($longestDuration_count->where('patientid', $pld['patientid'])->count() == 0){
    $creat = new Collection();
    $no_cre = $pld['created_at']->toDateString();
    foreach($sorted_pld as $sp){
        $cr = $sp['created_at']->toDateString();
        if($no_cre == $cr){
            $creat->push($sp);
        }
    }
        foreach($creat as $fi){
            $formula = ($criteria_p[1]->percentage / $to_pld) * $total_pld;
            $ld['rank'] = $total_pld_rank;
            $ld['created_at'] = $fi['created_at'];
            $ld['patientid'] = $fi['patientid'];
            $ld['patientname'] = $fi['patientname'];
            $ld['points'] = number_format($formula, 2, '.', ',');
            $longestDuration_count->push($ld);
        }
        $total_pld--;
        $total_pld_rank++;
    }
}
$longestDuration = $longestDuration_count->take(10);

$sorted_phn = $pnt_list->sortByDesc('lacking');
$highest_need_count = new Collection();
$total_phn = $sorted_phn->count();
$to_phn = $total_phn;
$total_phn_rank = 1;
foreach($sorted_phn as $p=>$phn){
    if($highest_need_count->where('patientid', $phn['patientid'])->count() == 0){
        $find = $sorted_phn->where('lacking', $phn['lacking']);
        foreach($find as $fi){
            $formula = ($criteria_p[2]->percentage / $to_phn) * $total_phn;
            // $percent = round((float)$formula * 100);
            $hn['rank'] = $total_phn_rank;
            $hn['patientid'] = $fi['patientid'];
            $hn['patientname'] = $fi['patientname'];
            $hn['points'] = number_format($formula, 2, '.', ',');
            $hn['lacking'] = $fi['lacking'];
            $highest_need_count->push($hn);
        }
        $total_phn--;
        $total_phn_rank++;
    }
}
$highest_need = $highest_need_count->take(10);

$sorted_c = $pnt_list->sortByDesc('condition');
$condition_count = new Collection();
$total_c = $sorted_c->count();
$to_c = $total_c;
$total_c_rank = 1;
foreach($sorted_c as $c){
    if($condition_count->where('condition', $c['condition'])->count() == 0){
        $find = $sorted_c->where('condition', $c['condition']);
        foreach($find as $fi){
            $formula = ($criteria_p[3]->percentage / $to_c) * $total_c;
            $con['rank'] = $total_c_rank;
            $con['patientid'] = $fi['patientid'];
            $con['condition'] = $fi['condition'];
            $con['patientname'] = $fi['patientname'];
            $con['points'] = number_format($formula, 2, '.', ',');
            $condition_count->push($con);
        }
        $total_c--;   
        $total_c_rank++;
    }
}
$condition = $condition_count->take(10);

$all_patients = new Collection();
foreach($longestDuration as $p_longest){
    $all_patients->push($p_longest);
}
foreach($highest_need as $p_highest){
    $all_patients->push($p_highest);
}
foreach($condition as $p_condition){
    $all_patients->push($p_condition);
}
foreach($patients as $patient){
    $all_patients->push($patient);
}


$final_list = new Collection();
foreach($all_patients as $d){
    $final = new Collection();
    if($final_list->where('patientid', $d['patientid'])->count() == 0){
    $total_r = Patient::select('TotalRedeem', 'userid')->where('patientid', $d['patientid'])->get();
    
        $find = $all_patients->where('patientid', $d['patientid']);
        foreach($find as $f){
            $final->push($f);
        }
        $f_pnt = null;
        $f_pnt['patientid'] = $d['patientid'];
        $f_pnt['patientname'] = $d['patientname'];
        $sum = $find->sum('points');
        $f_pnt['sum'] = number_format($sum, 2, '.', ',');
        $f_pnt['flag'] = 0;
        $f_pnt['TotalRedeem'] = $total_r[0]->TotalRedeem;
        $f_pnt['userid'] = $total_r[0]->userid;


foreach($final as $fin){
   
   if(array_key_exists('lacking', $fin)){
        $f_pnt['lacking'] = $fin['points'];
        $f_pnt['lacking_num'] = $fin['lacking'];
   }if(array_key_exists('condition', $fin)){
        $f_pnt['condition'] = $fin['points'];
   }if(array_key_exists('created_at', $fin)){
        $f_pnt['created_at'] = $fin['created_at'];
        $f_pnt['c_points'] = $fin['points'];
   }if(array_key_exists('count', $fin)){   
        $f_pnt['count'] = $fin['points'];
   }
}
        $final_list->push($f_pnt); 
    }
}

// $object = (object) $finalList;
// $this->getrecoresults($object);

$sorted_all = $final_list->sortByDesc('sum');
$total_rank = 1;
$same = new Collection();
foreach($sorted_all as $s=>$s_all){
    if($same->where('patientid', $s_all['patientid'])->count() == 0){
    $same_sum = $sorted_all->where('sum', $s_all['sum'])->sortBydesc('lacking');
    foreach($same_sum as $sa=>$ss){
        $ss['rank'] = $total_rank;
        $sorted_all[$sa] = $ss;
        $total_rank++;
        $same->push($ss);
    }
    }
}

// $finalList = new Collection();
// foreach($sorted_all as $all_p){

//     $all_p['flag'] = 1;
//     $finalList->push($all_p);return $all_p;
// }

// RECOMMENDATION
$t = new Collection();
    foreach($tohelpxp as $v){ 
        $cnt = 0;
        foreach($sorted_all as $p){
            if($p['flag'] == 1){
            $cnt++;
            }
        }
        if($cnt == $sorted_all->count()){
            foreach($sorted_all as $pp=>$p){
                $p['flag'] = 0; 
                $sorted_all[$pp] = $p;
            }
        }

    foreach($sorted_all as $pi=>$p){
        //return $p['lacking_num'];
       // $p['lacking_num'] = $p['lacking_num'];
        if(array_key_exists('lacking_num', $p)){

       
    if($p['lacking_num'] - $v->voucherValue >= 0 && $v->status == "d-hx" && $p['flag'] == 0){
            $p['lacking_num'] = $p['lacking_num'] - $v->voucherValue;
            $p['TotalRedeem'] = $p['TotalRedeem'] + $v->voucherValue;
            $v->status = 'dd-hx';
            $p['flag'] = 1;
            $sorted_all[$pi] = $p;
   
            $sponsor = Sponsor::findOrFail($v->sponsor_serial);
            $sponsor->status = 'dd-hx';
            $sponsor->save();

            $patient = Patient::findOrFail($p['patientid']);
            $newTotal = $patient->TotalRedeem + $v->voucherValue;
            $patient->TotalRedeem = $newTotal;
            $patient->flag = 1;
            $patient->save();

            $donation = new Donation();
            $donation->patientid = $p['patientid'];
            $donation->sponsor_serial = $v->sponsor_serial;
            $comName = $v->user->fname." ".$v->user->lname;
            $donation->sponsorName = $comName;
            $donation->save();
    }
}
    }
    }

foreach($sorted_all as $notif){
    $find_p = Patient::findOrFail($notif['patientid']);
    $story = $find_p->stories[0]->storytitle;
    $arr_add = array_add($find_p, 'storytitle', $story);
        if($find_p->goal == $find_p->TotalRedeem){
          $find_u = user::find($notif['userid']);
          \Notification::send($find_u, new patientNotification($notif));
        }
}

$final = $sorted_all->toArray();


$all_collection = new Collection();
$all_collection->push($patients);
$all_collection->push($longestDuration);
$all_collection->push($highest_need);
$all_collection->push($condition);
$all_collection->push($final);
$all_collection->push($count_helpxp);



return $all_collection;
}


public function rcmPatient(){
    // AppHelper::setreco();
    $sponsee = new Collection();
  $today = date('Y-m-d');
     $yesterday = date('Y-m-d', time() - 60 * 60 * 24);
    $donate = Donation::select('patientid')->distinct('patientid')->
    whereDate('created_at', $today)->get();
    foreach($donate as $sponsor=>$s){
        $total = 0;
        $ss = new Collection();
            $don_tbl = Donation::where('patientid', $s->patientid)->whereDate('created_at', $today)->get();
            $sponsee->push($don_tbl);
            foreach($don_tbl as $don){
                $spon_tbl = Sponsor::where('sponsor_serial', $don->sponsor_serial)->get();
                $total += $spon_tbl[0]->voucherValue; 
                $user = Patient::findOrFail($don->patientid);
                $s['patientid'] = $user->patientid;
                $s['patientname'] = $user->patientname;
            }
        $s['total'] = $total;
        $s['created_at'] = $don_tbl[$don_tbl->count()-1]->updated_at;
   }
   $donate = $donate->sortByDesc('total');

foreach($sponsee as $spon){
    foreach($spon as $s){
        $sponsor = Sponsor::findOrFail($s->sponsor_serial);
        $s['voucherValue'] = $sponsor->voucherValue;
        $s['name'] = $sponsor->user->fname." ".$sponsor->user->lname;
        $s['date'] = $sponsor->created_at;
    }  
}

  foreach($donate as $donor){
    foreach($sponsee as $spon){
        if($donor['patientid'] == $spon[0]->patientid){
            $donor['voucher'] = $spon;
        }
    }
  }
   $array = new Collection();
   foreach($donate as $result){
        $array->push($result);
   }
   return $array;
    

 // whereDate('created_at', $today)
    // $sponsored_pnt = DB::table('donations')
    // ->join('sponsors', 'donations.sponsor_serial', 'sponsors.sponsor_serial')
    // ->join('patients', 'donations.patientid', 'patients.patientid')
    // ->join('users as s', 's.id', 'sponsors.userid')
    // ->join('users as p', 'p.id', 'patients.userid')
    // ->where('sponsors.status', '=', 'dd-hx')
    // ->whereBetween(DB::raw('DATE(donations.created_at)'), array($today, $today))
    // ->select('donations.created_at', 's.fname', 's.lname', 'patients.patientname', 'sponsors.voucherValue', 'p.fname as userf', 'p.lname as userl')
    // ->get();
    // return $sponsored_pnt;

}

public function gcode(){
    $code = str_random(5);
    Session::put('oldCode',$code);
    $user = Auth::user();
    $user_arr = $user->toArray();
    $user_arr = array_add($user_arr,'code',$code);
    Mail::send('code', $user_arr, function($msg) use($user_arr){
        $msg->to(Auth::user()->email);
        $msg->subject('Registration Confirmation');
    });
    return $code;
}
public function gNewCode($code)
{ 
    $old_code = Session::get('oldCode');
    if($old_code == $code){
        return "true";
    }else{
        return $old_code;
    }
}


}
