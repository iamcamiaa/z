<?php

use Illuminate\Database\Seeder;

class CriteriaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $criteria = [
        	[
        		'name' => 'Most Patient Donated as Sponsor',
                'percentage' => '25'
       		],

            [
                'name' => 'Patient with Longest Duration',
                'percentage' => '25'
            ],
            [
                'name' => 'Patient with Highest Need',
                'percentage' => '25'
            ],
            [
                'name' => 'Patient based on Condition',
                'percentage' => '25'
            ]

       	];
       	DB::table('criterias')->insert($criteria);
    }
}
